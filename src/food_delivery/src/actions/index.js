import { 
    LOAD_RESTAURANTS, 
    LOAD_MENU, 
    SEARCH_RESTAURANTS, 
    ADD_ITEM, 
    DELETE_ITEM,
    RESET_BILL,
    CHANGE_LANGUAGE, 
    LOGIN,
    GET_ACCOUNT } from './define';

export const loadRestaurants = res =>({
    type: LOAD_RESTAURANTS,
    res
})

export const loadMenu = menu => ({
    type: LOAD_MENU,
    menu
})

export const addItem = item => ({
    type: ADD_ITEM,
    item
})

export const deleteItem = item => ({
    type: DELETE_ITEM,
    item
})

export const resetBill = () => ({
    type: RESET_BILL,
})

export const changeLanguage = language => ({
    type: CHANGE_LANGUAGE,
    language
})

export const login = token => ({
    type: LOGIN,
    token
})

export const getAccountInfo = data =>({
    type: GET_ACCOUNT,
    data
})