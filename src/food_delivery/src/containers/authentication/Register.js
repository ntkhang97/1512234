import React, { Component } from 'react';
import { View, Text, ImageBackground } from 'react-native';
import Input from './Input';
import SubmitBtn from './SubmitBtn';
import PasswordInput from './PasswordInput';
import { register } from '../../services/api';
import { res_styles } from './Res-Styles';
import { styles } from './Styles';

export class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        }
    }
    handleSubmit() {
        register(this.state.email, this.state.password);
    }
    render() {
        return (
            <View style={res_styles.mainContainer}>
                <ImageBackground
                    source={require('../../images/background/homebackground.png')}
                    resizeMode='cover'
                    blurRadius={5}
                    style={{ width: '100%', height: '100%' }}>
                    <View style={[res_styles.titleContainer,]}>
                        <View style={{ flex: 1, flexDirection: 'column-reverse' }}>
                            <Text style={styles.titleStyle}>Register</Text>
                        </View>
                    </View>

                    <View style={res_styles.formContainer}>
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <Input placeholder='Enter your email address' onChangeText={(text) => this.setState({ email: text })} />
                            <PasswordInput placeholder='Enter your password' onChangeText={(text) => this.setState({ password: text })} />
                        </View>
                    </View>

                    <View style={[res_styles.submitContainer,{ marginTop: 30 }]}>
                        <View style={{ flex: 1, alignItems: 'center', paddingTop: 15 }}>
                            <SubmitBtn text='Submit' onPress={() => this.handleSubmit()} />
                        </View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}