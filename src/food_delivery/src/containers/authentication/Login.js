import React, { Component } from 'react';
import { View, Text, ImageBackground } from 'react-native';
import Input from './Input';
import PasswordInput from './PasswordInput';
import SubmitBtn from './SubmitBtn';
import { login } from '../../services/api';
import { styles } from './Styles';
import { connect } from 'react-redux';
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        }
    }
    handleSubmit = async () => {
        login(this.state.email,this.state.password, this);
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                <ImageBackground
                    source={require('../../images/background/homebackground.png')}
                    resizeMode='cover'
                    blurRadius={5}
                    style={{ width: '100%', height: '100%' }}>
                    
                    <View style={[styles.titleContainer, { flex: 1, flexDirection: 'column-reverse' }]}>
                        <Text style={styles.titleStyle}>Food Delivery</Text>
                    </View>

                    <View style={styles.formContainer}>
                        <Input placeholder='Enter your username'
                            value={this.state.email}
                            onChangeText={(text) => this.setState({ email: text })} />
                        <PasswordInput placeholder='Enter your password' onChangeText={(text) => this.setState({ password: text })} />
                    </View>

                    <View style={[styles.submitContainer, { marginTop: 30 }]}>
                        <SubmitBtn text='Login' onPress={() => this.handleSubmit()} />
                    </View>
                </ImageBackground>
            </View >
        );
    }
}

export default connect()(Login);