import React from 'react';
import { StyleSheet} from 'react-native';

export const res_styles = StyleSheet.create({
    titleContainer: {
        height: '40%'
    },
    formContainer: {
        height: '20%',
    },
    submitContainer: {
        height: '40%',
    },
});