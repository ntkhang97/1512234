import React, {Component} from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import {styles} from './Styles';
export default class SubmitBtn extends Component {
    render() {
        return (
            <TouchableOpacity style={styles.btnStyle} onPress={() => this.props.onPress()}>
                <View style={styles.btnView}>
                    <Text style={styles.btnText}>
                        {this.props.text}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}