import React, { Component } from 'react';
import { View, Text , ImageBackground} from 'react-native';
import Input from './Input';
import SubmitBtn from './SubmitBtn';
import { styles } from './Styles';
import { forgetPassword } from '../../services/api';
export class ForgetPassword extends Component {
    state = {
        email: ''
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                <ImageBackground
                    source={require('../../images/background/homebackground.png')}
                    resizeMode='cover'
                    blurRadius={5}
                    style={{ width: '100%', height: '100%' }}>
                    <View style={[styles.titleContainer, { flex: 1, flexDirection: 'column-reverse' }]}>
                        <Text style={styles.titleStyle}>Forget Password</Text>
                    </View>

                    <View style={styles.formContainer}>
                        <Input placeholder='Enter your email' 
                        value={this.state.email}
                        onChangeText={(text)=>this.setState({email: text})}
                        />
                    </View>

                    <View style={[styles.submitContainer, { marginTop: 30 }]}>
                        <SubmitBtn text='Submit' onPress={()=>forgetPassword(this.state.email)}/>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}