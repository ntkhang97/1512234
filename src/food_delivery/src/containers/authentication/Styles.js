import React from 'react';
import { StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    mainContainer: {
        height: '100%'
    },
    titleContainer: {
        height: '30%'
    },
    titleStyle: {
        color: 'white',
        fontSize: 44,
        textAlign: 'center',
        fontFamily: 'gabriola'
    },
    formContainer: {
        height: '40%',
        flex: 1,
        justifyContent: 'center'
    },
    textIpView: {
        borderTopWidth: 0,
        borderBottomWidth: 2,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: 'white',
        marginLeft: 40,
        marginRight: 40,
        marginTop: 5
    },
    textIp: {
        fontSize: 18,
        color: 'white',
        fontFamily: 'gabriola',
        textAlign: 'center',
        height: 50
    },
    submitContainer: {
        flex: 1,
        alignItems: 'center',
        height: '30%',
    },
    btnStyle: {
        backgroundColor: '#BD7207',
        width: 95,
        height: 34,
        borderRadius: 10,
        borderWidth: 0,
        opacity: 0.72
    },
    btnText: {
        fontSize: 20,
        color: 'white',
        textAlign: 'center',
        fontFamily: 'gabriola'
    },
    btnView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});