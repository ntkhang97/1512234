import React, { Component } from 'react';
import { View, TextInput } from 'react-native';
import {styles} from './Styles';

export default class Input extends Component {
    render() {
        return (
            <View style={[styles.textIpView,]}>
                <TextInput style={[styles.textIp,]}
                    placeholder={this.props.placeholder} 
                    value={this.props.value} 
                    placeholderTextColor='white'
                    onChangeText={(text) => 
                    this.props.onChangeText(text)}>
                </TextInput>
            </View>
        );
    }
}