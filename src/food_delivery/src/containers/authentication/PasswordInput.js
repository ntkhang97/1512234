import React, { Component } from 'react';
import { View, TextInput } from 'react-native';
import {styles} from './Styles';

export default class PasswordInput extends Component {
    render() {
        return (
            <View style={styles.textIpView}>
                <TextInput 
                secureTextEntry={true} 
                style={styles.textIp} 
                placeholder={this.props.placeholder} 
                onChangeText={(text) => this.props.onChangeText(text)}
                placeholderTextColor='white'></TextInput>
            </View>
        );
    }
}