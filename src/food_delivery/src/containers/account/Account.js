import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity, ActivityIndicator, Alert, Dimensions, BackHandler } from 'react-native';
import {
    ListItem, Title, Body, Separator, Form, Input, Left, Icon,
    Right, List, Button, Header, Content, Label, Textarea, Container, Item, Text,
    Picker
} from 'native-base';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Avatar from './Avatar';
import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";
import SplashScreen from 'react-native-splash-screen';
import { Actions } from 'react-native-router-flux';
import Permissions from 'react-native-permissions';
import { getAccountInfo, getAllDistrict, getAllWards, 
    updateAccountInfo, updateAddressInfo, updateAddressPassword } from '../../services/api';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import I18n from 'react-native-i18n';

const style = {
    iconColor: '#32936F'
}
const COLOR = '#32936F';
class Account extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isShowPass: false,
            idDistrict: 13109,
            ward: 9279,
            street: '',
            photoPermission: null,
            cameraPermission: null,
            isLoading: false,
            districts: null,
            wards: null,
            userName: '',
            phone: '',
            newPassword: '',
            confirmPassword: '',
            avatarSource:''
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }
    onDistrictChange(value) {
        this.setState({
            idDistrict: parseInt(value)
        });
        this.getWard(parseInt(value));
    }
    onWardChange(value) {
        this.setState({
            ward: value
        });
    }
    onStreetChange(value) {
        this.setState({
            street: value
        });
    }
    async handleChangeAvatar() {
        // await this._requestPermission('photo');
        // await this._requestPermission('camera');

        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        // ImagePicker.showImagePicker(options, (response) => {
        //     console.log('Response = ', response);

        //     if (response.didCancel) {
        //         console.log('User cancelled image picker');
        //     } else if (response.error) {
        //         console.log('ImagePicker Error: ', response.error);
        //     } else if (response.customButton) {
        //         console.log('User tapped custom button: ', response.customButton);
        //     } else {
        //         const source = { uri: response.uri };

        //         // You can also display the image using data:
        //         // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        //         this.setState({
        //             avatarSource: source,
        //         });
        //     }
        // });

        ImagePicker.launchImageLibrary(options, (response) => {
            // Same code as in above section!
        });
    }

    _requestPermission(type) {
        Permissions.request(type).then(response => {
            // console.log('Location permission: ', response);
            this.setState({ picturePermission: response })

        })
    }

    _checkCameraAndPhotos = async () => {
        await Permissions.checkMultiple(['camera', 'photo']).then(response => {
          //response is an object mapping type to permission
          this.setState({
            cameraPermission: response.camera,
            photoPermission: response.photo,
          })
        })
      }

    componentDidMount() {

        this.setState({ isLoading: true });
        this.getAllData();
        this._checkCameraAndPhotos()
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    handleBackButtonClick() {
        Alert.alert(
            I18n.t('notification'),
            I18n.t('doExit'),
            [
                {
                    text: 'Ok', onPress: ()=>{
                        BackHandler.exitApp();
                    }
                },
                {
                    text: 'Cancel', style: 'cancel'
                }
            ],
            { cancelable: true }
        )
        return true;
    }

    getAllData = async () => {
        await this.getInfo();
        this.getDistricts();
        
        this.getWard(this.state.idDistrict);

        this.setState({
            userName: this.props.data?this.props.data.userName:'',
            phone: this.props.data?this.props.data.phone:'',
            street: this.props.data?this.props.data.address.street:'',
            idDistrict: this.props.data?this.props.data.address.idDistrict:'',
            ward: this.props.data?this.props.data.address.ward:'',
            avatarSource: this.props.data?this.props.data.avatarUrl:''
        })
    }

    getInfo = async () => {
        const data = await getAccountInfo(this.props.token);
        this.setState({isLoading: false})
        // console.log('ACCOUNT DATA: ', data);
        this.props.dispatch({ type: 'GET_ACCOUNT', data: data });
    }

    getDistricts = async () => {
        const data = await getAllDistrict();
        // console.log('All District', data);
        this.setState({districts: data});
    }

    getWard = async (id) => {
        const data = await getAllWards(id);
        // console.log('All ward in District');
        this.setState({wards: data})
    }

    loadAllDistrict() {
        const districts = this.state.districts;
        //  console.log('District in loading', districts);
        if(districts !== null) {
            return districts.map(d=>{
                return <Picker.Item key={d.id} label={d.name} value={d.id} />
            })
        }
    }

    onPressUpdateAccount() {
        const token = this.props.token;
        const userName = this.state.userName;
        const phone = this.state.phone;
        if(token) {
            if(userName!='' && phone!='') {
                updateAccountInfo(token,userName,phone).then(this.processResponse);
            }
        }
    }

    onPressUpdateAddress() {
        const token = this.props.token;
        const idDistrict = this.state.idDistrict;
        const ward = this.state.ward;
        const street = this.state.street;
        if(token) {
            if(idDistrict!='' && ward!='' && street != '') {
                updateAddressInfo(token,idDistrict,ward,street).then(this.processResponse);
            }
        }
    }

    onPressChangePassword() {
        const token = this.props.token;
        const newPass = this.state.newPassword;
        const confirmPass = this.state.confirmPassword;
        if(newPass != confirmPass) {
            Alert.alert(I18n.t('notification'), I18n.t('newPassAndOldPassDiff'));
            return;
        }
        if(token) {
            if(newPass!='' && confirmPass!='') {
                updateAddressPassword(token, newPass).then(this.processResponse);
            }
        }
    }

    processResponse = res=>{
        Alert.alert(I18n.t('notification'), res.data.msg);
    }

    loadAllWard() {
        const wards = this.state.wards;
        
        if(wards !== null) {
            return wards.map(w=>{
                return <Picker.Item key={w.id} label={w.name} value={w.id} />
            })
        }
    }

    render() {
        return (
            <Container style={{ height: '100%' }}>
                <Header style={{ backgroundColor: style.iconColor }}>
                    <Body>
                        <Title style={{ fontSize: hp('2.5%') }}>{I18n.t('account')}</Title>
                    </Body>
                    {/* <Right>
                        <Button transparent onPress={() => Actions.listOrder()}>
                            <Icon name='list' />
                        </Button>
                    </Right> */}
                </Header>
                
                <Content>{
                    this.state.isLoading ? 
                    <View style={{
                        height: Dimensions.get('window').height-120, 
                        width: Dimensions.get('window').width, 
                        backgroundColor: 'white',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center'}}>
                        <ActivityIndicator size="large" color="#32936F" />
                    </View> :
                    <View>
                        <View style={{
                            display: 'flex',
                            alignItems: 'center',
                            margin: 10
                        }}>
                            <Avatar uri={this.state.avatarSource}/>
                            <TouchableOpacity onPress={() => this.handleChangeAvatar()}>
                                <Text>{I18n.t('changeAvatar')}</Text>
                            </TouchableOpacity>

                        </View>
                        <Collapse isCollapsed={true}>
                            <CollapseHeader style={{ borderBottomWidth: 2, borderColor: style.iconColor }}>
                                <Text style={{ fontSize: hp('2.5%'), color: COLOR}}>{I18n.t('accountInformation')}</Text>
                            </CollapseHeader>

                            <CollapseBody>
                                <Form>
                                    <Item>
                                        <Icon active name='person' style={{ color: style.iconColor }} />
                                        <Input placeholder='Username' 
                                        value={this.state.userName}
                                        onChangeText={(text)=>{this.setState({userName: text})}}
                                        />
                                    </Item>
                                    <Item>
                                        <Icon active name='mail' style={{ color: style.iconColor }} />
                                        <Input disabled placeholder='Email' 
                                        value={this.props.data?this.props.data.email:''}/>
                                    </Item>
                                    <Item>
                                        <Icon active name='phone-portrait' style={{ color: style.iconColor }} />
                                        <Input placeholder='Phone' value={this.state.phone}
                                        onChangeText={(text)=>{this.setState({phone: text})}}
                                        />
                                    </Item>
                                    <Button block style={{backgroundColor: '#32936F'}} onPress={()=>this.onPressUpdateAccount()}>
                                        <Text>{I18n.t('updateAccount')}</Text>
                                    </Button>
                                </Form>
                            </CollapseBody>
                        </Collapse>
                        <Collapse isCollapsed={true} style={{ marginTop: 10 }}>
                            <CollapseHeader style={{ borderBottomWidth: 2, borderColor: style.iconColor }}>
                                <Text style={{ fontSize: hp('2.5%'), color: COLOR}}>{I18n.t('addressInformation')}</Text>
                            </CollapseHeader>

                            <CollapseBody>
                                <Form>
                                    <Label>{I18n.t('district')}</Label>
                                    <Item picker>
                                        <Picker
                                            mode="dropdown"
                                            iosIcon={<Icon name="ios-arrow-down-outline" />}
                                            style={{ width: undefined }}
                                            placeholder="Select district"
                                            placeholderStyle={{ color: "#bfc6ea" }}
                                            placeholderIconColor="#007aff"
                                            selectedValue={this.state.idDistrict}
                                            onValueChange={this.onDistrictChange.bind(this)}
                                        >
                                            {this.loadAllDistrict()}
                                        </Picker>
                                    </Item>
                                    <Label>{I18n.t('ward')}</Label>
                                    <Item picker>
                                        <Picker
                                            mode="dropdown"
                                            iosIcon={<Icon name="ios-arrow-down-outline" />}
                                            style={{ width: undefined }}
                                            placeholder="Select ward"
                                            placeholderStyle={{ color: "#bfc6ea" }}
                                            placeholderIconColor="#007aff"
                                            selectedValue={this.state.ward}
                                            onValueChange={this.onWardChange.bind(this)}
                                        >
                                            {this.loadAllWard()}
                                        </Picker>
                                    </Item>
                                    <Label>{I18n.t('street')}</Label>
                                    <Item>
                                        <Input placeholder='Street' value={this.state.street}
                                        onChangeText={(text)=>{this.setState({street: text})}}
                                        />
                                    </Item>
                                    <Button block style={{backgroundColor: '#32936F'}} onPress={()=>this.onPressUpdateAddress()}>
                                        <Text>{I18n.t('updateAddress')}</Text>
                                    </Button>
                                </Form>
                            </CollapseBody>
                        </Collapse>

                        <Collapse isCollapsed={true} style={{ marginTop: 10 }}>
                            <CollapseHeader style={{ borderBottomWidth: 2, borderColor: style.iconColor }}>
                                <Text style={{ fontSize: hp('2.5%'), color: COLOR}}>{I18n.t('changePassword')}</Text>
                            </CollapseHeader>

                            <CollapseBody>
                                <Form>
                                    <Item>
                                        <Input placeholder={I18n.t('newPassword')} secureTextEntry={!this.state.isShowPass} 
                                            onChangeText={(text)=>this.setState({newPassword: text})}
                                        />
                                        <Icon active name={this.state.isShowPass ? 'eye' : 'eye-off'}
                                            style={{ color: style.iconColor }}
                                            onPress={() => this.setState({ isShowPass: !this.state.isShowPass })}
                                        />
                                    </Item>
                                    <Item>
                                        <Input placeholder={I18n.t('confirmPassword')} secureTextEntry={!this.state.isShowPass} 
                                            onChangeText={(text)=>this.setState({confirmPassword: text})}
                                        />
                                        <Icon active name={this.state.isShowPass ? 'eye' : 'eye-off'}
                                            style={{ color: style.iconColor }}
                                            onPress={() => this.setState({ isShowPass: !this.state.isShowPass })}
                                        />
                                    </Item>
                                    <Button block style={{backgroundColor: '#32936F'}} onPress={()=>this.onPressChangePassword()}>
                                        <Text>{I18n.t('confirmChangePassword')}</Text>
                                    </Button>
                                </Form>
                            </CollapseBody>
                        </Collapse>
                    </View>
                }
                </Content>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        token: state.token,
        data: state.account
    }
}
export default connect(mapStateToProps)(Account);