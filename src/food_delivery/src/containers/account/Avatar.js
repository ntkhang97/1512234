import React, { Component } from 'react';
import {View, ImageBackground, StyleSheet, Dimensions} from 'react-native';
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';
class Avatar extends Component {
    render() {
        return (
           <View style={{
               height: hp('20%'),
               width:hp('20%'),
               backgroundColor: 'white',
               borderRadius:hp('10%'),
               borderColor: 'green',
               borderWidth: 2,
               display: 'flex',
               justifyContent: 'center',
               alignItems: 'center'
           }}>
                <View style={{
                    height: hp('17%'),
                    width:hp('17%'),
                    borderRadius:hp('8.5%'),
                    overflow: 'hidden'
                }}
                    >
                    <ImageBackground 
                        source={{uri: this.props.uri?this.props.uri:'https://raw.githubusercontent.com/Ashwinvalento/cartoon-avatar/master/lib/images/male/45.png'}} 
                        style={{width: '100%',height: '100%'}}
                        resizeMethod='resize'
                        >
                    
                    </ImageBackground>
                </View>
           </View>
        );
    }
}

export default Avatar;