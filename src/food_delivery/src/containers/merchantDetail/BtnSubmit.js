import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';
const COLOR = '#32936F';
export class BtnSubmit extends Component {
    render() {
        return (
            <TouchableOpacity style={styleBtn.footBtnFrame} onPress={() => this.props.onPress()}>
                <View style={styleBtn.footBtnView}>
                    <Text style={styleBtn.footBtnText}>{this.props.text}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styleBtn = StyleSheet.create({
    footBtnFrame: {
        backgroundColor: COLOR,
        width: '100%',
        height: '100%',
        borderRadius: 10
    },
    footBtnView: {
        flex: 1,
        justifyContent: 'center'
    },
    footBtnText: {
        fontSize: hp('2%'),
        color: 'white',
        textAlign: 'center'
    }
})