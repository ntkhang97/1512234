import React, { Component } from 'react';
import { Alert, View, Text, ActivityIndicator, ImageBackground, BackHandler } from 'react-native';
import { Actions } from 'react-native-router-flux';
import ListItems from '../../components/listItemsInRestaurant/ListItems';
import { connect } from 'react-redux';
import { BtnSubmit } from './BtnSubmit';
import {styleMercDetail} from './StyleMercDetail';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5Icons from 'react-native-vector-icons/FontAwesome5';
import { loadMenuInRestaurant } from '../../services/api';
import I18n from 'react-native-i18n';
import {Icon} from 'native-base';

class MerchantDetail extends Component {
    state = {
        isLoading: true
    }
    componentDidMount(){
        this.loadData();
    }

    loadData = async () => {
        const data = await loadMenuInRestaurant(this.props.id);
        this.setState({isLoading: false});
        this.props.dispatch({type: 'LOAD_MENU', menu: data});
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styleMercDetail.headContainer}>
                    <ImageBackground
                        source={this.props.uri ? { uri: this.props.uri } : require('../../images/imgs/place04.png')} style={{ width: '100%', height: '90%' }}>
                        <View style={styleMercDetail.topHeader}>
                            <Icon name='ios-arrow-back' style={{color: 'white'}}
                                onPress={()=>Actions.merchant()}
                                />
                        </View>
                    </ImageBackground>

                    <View style={styleMercDetail.bottomContainer}>
                        <View style={styleMercDetail.titleFrame}>
                            <View style={{
                                width: '100%',
                                height: '40%',
                            }}>
                                <Text style={styleMercDetail.titleText}>
                                    {this.props.title ? this.props.title : 'title'}
                                </Text>
                            </View>
                            {/* <View style={{
                                width: '100%',
                                height: '30%',
                            }}>
                                <Text style={styleMercDetail.kindText}>
                                    {this.props.kind ? this.props.kind : I18n.t('kind')}
                                </Text>
                            </View>
                            <View style={{
                                width: '100%',
                                height: '30%',
                            }}>
                                <Text style={styleMercDetail.timeText}>
                                    {this.props.time ? this.props.time : I18n.t('time')}
                                </Text>
                            </View> */}
                        </View>
                    </View>
                </View>

                <View style={styleMercDetail.timeContainer}>
                    <View style={styleMercDetail.timeFrame}>
                        <View style={styleMercDetail.timeTitle}>
                            <Text style={styleMercDetail.timeTitleText}>{I18n.t('openTime')}</Text>
                            <Text style={styleMercDetail.timeTitleText}>{I18n.t('closeTime')}</Text>
                        </View>
                        <View style={styleMercDetail.timeDetail}>
                            <Text style={styleMercDetail.timeDetailText}>{I18n.t('from2to7')}</Text>
                            <Text style={styleMercDetail.timeDetailText}>{I18n.t('from6to9')}</Text>
                        </View>
                    </View>
                </View>

                <View style={styleMercDetail.mainContainer}>

                    {
                        this.state.isLoading || this.props.id === null || this.props.id === undefined ? <ActivityIndicator size="large" color="#32936F"/>
                        : <ListItems id={this.props.id} />
                    }
                </View>

                <View style={styleMercDetail.footContainer}>
                    <View style={styleMercDetail.footFrame}>
                        <BtnSubmit
                            text={`${I18n.t('seeTheCart')}: ${this.props.numberItems} ${I18n.t('items')}, ${this.props.cost} VND`}
                            onPress={() => Actions.basket({ title: this.props.title })} />
                    </View>
                </View>
            </View>
        );
    }
}
const mapStateToProps = state => {
    // console.log('in map state to props of bill');
    // console.log(state.bill);
    // console.log(state.bill === null);
    if (state.bill === null)
        return {
            numberItems: 0,
            cost: 0
        }
    else {
        const cost = state.bill.items.reduce((accumulator, currentItem) => accumulator + currentItem.cost*currentItem.numbers, 0);
        const numberItems = state.bill.items.reduce((accumulator, currentItem) => accumulator + currentItem.numbers, 0);
        // console.log(cost);
        return {
            numberItems,
            cost
        }
    }
}
export default connect(mapStateToProps)(MerchantDetail);