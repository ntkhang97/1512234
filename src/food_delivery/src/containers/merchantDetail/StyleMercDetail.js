import React from 'react';
import { StyleSheet } from 'react-native';
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';
const COLOR = '#BD7207'
export const styleMercDetail = StyleSheet.create({
    headContainer: {
        width: '100%',
        height: hp('25'),
    },
    topHeader: {
        width: '100%',
        height: hp('6%'),
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
        paddingTop: 10,
        paddingLeft: 20,
        paddingRight: 20
    },
    bottomContainer: {
        height: 100,
        width: '100%',
        position: 'absolute',
        bottom: 0,
        flex: 0,
        alignItems: 'center',
    },
    titleFrame: {
        height: '100%', 
        minWidth: 238,
        maxWidth: '60%',
        backgroundColor: 'white',
        borderRadius: 5,
        paddingLeft: 10,
        borderWidth: 1,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#ddd',
        shadowOffset: { width: 0, height: 10 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 10,
    },
    titleText: {
        color: COLOR,
        fontWeight: 'bold',
        textTransform: 'capitalize',
        fontSize: hp('2.5%'),
        textAlign: 'left'
    },
    kindText: {
        color: '#707070',
        fontSize: hp('2%'),
        textAlign: 'left',
        marginTop: 15
    },
    timeText: {
        color: '#707070',
        fontSize: hp('1.5%'),
        textAlign: 'left',
        fontWeight: 'bold'
    },
    timeContainer: {
        height: 50,
    },
    timeFrame: {
        flex: 0,
        flexDirection: 'row',
        width: '100%',
        height: '100%',
        borderBottomColor: '#707070',
        borderBottomWidth: 1,
    },
    timeTitle: {
        width: '40%',
        height: '100%',
        flex: 0,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 10
    },
    timeDetail: {
        width: '60%',
        height: '100%',
        flex: 0,
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 10
    },
    timeTitleText: {
        fontSize: hp('1.5%'),
        color: '#707070',
        fontWeight: 'bold'
    },
    timeDetailText: {
        fontSize: hp('1.5%'),
        color: '#707070',
        textAlign: 'left'
    },
    mainContainer: {
        height: '50%'
    },
    footContainer: {
        width: '100%',
        height: 30,
        position: 'absolute',
        bottom: 0
    },
    footFrame: {
        height: '100%',
        width: '100%',
        paddingLeft: 20,
        paddingRight: 20,
    },
});