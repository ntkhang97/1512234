import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {styles} from './home-style';
import { Actions } from 'react-native-router-flux';

export default class Register extends Component {
    render() {
        return (
            <TouchableOpacity style={[styles.rect, styles.btnSize,
            {
                backgroundColor: 'transparent',
                borderWidth: 2,
                borderColor: 'white'
            }]} onPress = {()=>Actions.register()}>
                <View style={styles.btnView}>
                    <Text 
                    style={[styles.btnText, styles.text, { color: 'white' }]}
                    
                    >
                    {this.props.text}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}