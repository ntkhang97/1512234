import React, {Component} from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {styles} from './home-style';
import { Actions } from 'react-native-router-flux';

export default class ForgetPassword extends Component {
    render() {
        return (
            <TouchableOpacity onPress={()=>Actions.forgetPassword()}>
                <Text style={[styles.text, styles.intro]}>Forget password</Text>
            </TouchableOpacity>
        );
    }
}