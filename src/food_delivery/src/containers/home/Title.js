import React, {Component} from 'react';
import {Text} from 'react-native';
import {styles} from './home-style';

export default class Title extends Component {
    render() {
        return (
            <Text style={[styles.rect, styles.text, styles.title]}>{this.props.text}</Text>
        );
    }
}