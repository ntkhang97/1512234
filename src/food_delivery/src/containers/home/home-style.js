import React from 'react';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    mainContainer:{
        height:'100%'
    },
    rect: {
        backgroundColor: 'white',
        borderRadius: 10,
        borderWidth: 0,
        opacity: 0.82
    },
    text: {
        fontFamily: 'gabriola',
        textAlign: 'center'
    },
    title: {
        color: '#5A1717',
        fontSize: 44,
        marginLeft: 40,
        marginRight: 40
    },
    intro: {
        color: '#fff',
        fontSize: 20
    },
    btnText: {
        color: '#5A1717',
        fontSize: 20,
    },
    btnSize: {
        width: 95,
        height: 34,
        marginLeft: 5,
        marginRight: 5
    },
    btnView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});