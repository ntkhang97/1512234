import React, { Component } from 'react';
import { Text, View, ImageBackground, BackHandler, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {getUserToken} from '../../services/authentication';
import {styles} from './home-style';
import Title from './Title';
import Login from './LoginBtn';
import Register from './RegisterBtn';
import ForgetPassword from './ForgetPassBtn';
import SplashScreen from 'react-native-splash-screen'
import {connect} from 'react-redux';
import I18n from 'react-native-i18n';
class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
            hasToken: false
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    handleBackButtonClick = () => {
        Alert.alert(
            I18n.t('notification'),
            I18n.t('doExit'),
            [
                {
                    text: 'Ok', onPress: ()=>{
                        BackHandler.exitApp();
                    }
                },
                {
                    text: 'Cancel', style: 'cancel'
                }
            ],
            { cancelable: true }
        )
        return true;
    }
    componentDidMount(){
        this.getAuthen();
        //setTimeout(()=>SplashScreen.hide(),2000); 
    }

    // componentWillMount(){
    //     this.getAuthen();
    //     //setTimeout(()=>SplashScreen.hide(),2000); 
    // }

    getAuthen = async () => { 
        const token = await this.props.token;
        console.log('Token: ', token);
        if(token!==null) {
            Actions.merchant();
        }
        SplashScreen.hide();
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                <ImageBackground
                    source={require('../../images/background/homebackground.png')} style={{ width: '100%', height: '100%' }}>
                    <View>
                        <View style={{ height: '30%' }}>
                            <View style={{ flex: 1, flexDirection: 'column-reverse' }}>
                                <Title text='Food Delivery' />
                            </View>
                        </View>

                        <View style={{ height: '35%', marginTop: 20 }}>
                            <Text style={[styles.text, styles.intro]}>Get the food you want</Text>
                            <Text style={[styles.text, styles.intro]}>Just stay at home, we will bring it to you</Text>
                        </View>

                        <View style={{ height: '35%' }}>
                            <View style={{ height: '20%' }}>
                                <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                                    <Login text='Log in' />
                                    <Register text='Register' />
                                </View>
                            </View>
                            <View style={{ height: '30%', paddingTop: 10 }}>
                                <ForgetPassword />
                            </View>
                            <View style={{ height: '50%' }}>
                                <Text style={{
                                    fontSize: 16,
                                    color: 'white',
                                    textAlign: 'center'
                                }}>
                                    Copyright by Nguyen Thinh Khang
                                    </Text>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}
const mapStateToProps = state => ({
    token: state.token
})

export default connect(mapStateToProps)(Home);
