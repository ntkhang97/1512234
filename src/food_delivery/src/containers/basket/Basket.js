import React, { Component } from 'react';
import { View, Alert, BackHandler } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { BtnSubmit } from '../merchantDetail/BtnSubmit';
import { Item as FoodItem } from './Item';
import { connect } from 'react-redux';
import {
    ListItem, Title, Body, Separator, Form, Input, Left, Icon,
    Right, Toast, Button, Header, Content, Label, Textarea, Container, Item, Text
} from 'native-base';
import AwesomeAlert from 'react-native-awesome-alerts'
import I18n from 'react-native-i18n';
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {getDistrictName, getWardName, createOrder} from '../../services/api'
const COLOR  = '#32936F';
class Basket extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            showAlert: false,
            districtName: '',
            wardName: '',
            street: ''
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    };

    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };

    loadItem() {
        const bill = this.props.bill;
        if (bill === null)
            return;
        else {
            return bill.map(item => {
                return <FoodItem key={item.id} numbers={item.numbers} name={item.title} cost={item.cost} />
            })
        }
    }

    componentDidMount() {
        this.loadInfo();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        Alert.alert(
            I18n.t('notification'),
            I18n.t('doExit'),
            [
                {
                    text: 'Ok', onPress: ()=>{
                        BackHandler.exitApp();
                    }
                },
                {
                    text: 'Cancel', style: 'cancel'
                }
            ],
            { cancelable: true }
        )
        return true;
    }

    loadInfo = async () => {
        const idDistrict = this.props.account ? this.props.account.address.idDistrict : '';
        const idWard = this.props.account ? this.props.account.address.idWard: '';
        console.log('DName,Wname', this.props.account);
        if(idDistrict != '' && idWard != '') {
            const dName = await getDistrictName(idDistrict);
            const wName = await getWardName(idWard, idDistrict);
            console.log('DName,Wname', dName, wName);
            this.setState({street: this.props.account.address.street,districtName: dName, wardName: wName});
        }
    }

    renderAlert = (_title, _message) => {
        return (<AwesomeAlert
            show={this.state.showAlert}
            showProgress={false}
            title={_title}
            message={_message}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={false}
            showCancelButton={true}
            showConfirmButton={true}
            cancelText={I18n.t('noCancel')}
            confirmText={I18n.t('yesDoIt')}
            confirmButtonColor="#388E3C"
            onCancelPressed={() => {
                this.hideAlert();
            }}
            onConfirmPressed={() => {
                this.hideAlert();
                this.onCreateOrder();
            }}
        />)
    }

    getAddress() {
        return `${this.state.street}, ${this.state.wardName}, ${this.state.districtName}`
    }

    onCreateOrder() {
        const token = this.props.token;
        if(token) {
            const data = {
                totalPrice: this.props.totalCost*1.1 + 10000,
                Street: this.props.account.address.street,
                idWard: this.props.account.address.idWard,
                idDistrict: this.props.account.address.idDistrict,
                phone: this.props.account.phone,
                idRestaurant: this.props.resId,
                item: this.props.bill.map(b=>{
                    return {
                        idFood: b.id,
                        quantity: b.numbers,
                        note: 'no note'
                    }
                })
            }
            createOrder(token,data).then(res=>{
                if(res.status == 200) {
                    Alert.alert('Notification', 'Created order successfully');
                    this.props.dispatch({type: 'RESET_BILL'});
                    Actions.listOrder();
                }
            })
        }
    }

    render() {
        return (
            <Container>
                <Header noLeft style={{backgroundColor: COLOR}}>
                    <Left>
                        <Button transparent>
                            <Icon name='home' />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={{fontSize: hp('2.5%')}}>{I18n.t('basket')}</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => Actions.listOrder()}>
                            <Text>{I18n.t('listOrder')}</Text>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <Separator bordered>
                        <Label style={{fontSize: hp('2%')}}>{I18n.t('address')}</Label>
                    </Separator>

                    <Text style={{fontSize: hp('2%'), color: COLOR}}>{I18n.t('yourAddress') + ':' + this.getAddress()}</Text>
                    <Form>
                        <Item floatingLabel>
                            <Label style={{fontSize: hp('2%')}}>{I18n.t('deliveryToOrder')}</Label>
                            <Input />
                        </Item>
                        <Item floatingLabel last>
                            <Label style={{fontSize: hp('2%')}}>{I18n.t('moreDescription')}</Label>
                            <Textarea rowSpan={5} bordered />
                        </Item>
                    </Form>
                    <Separator bordered>
                        <Label style={{fontSize: hp('2%')}}>{I18n.t('items')}</Label>
                    </Separator>
                    {this.loadItem()}
                    <ListItem style={{ height: 60 }}>
                        <BtnSubmit text={I18n.t('changeItems')} onPress={() => {
                            if (this.props.bill === null) {
                                console.log('Show toast')
                                Toast.show({
                                    text: I18n.t('youShouldPickSthFirst'),
                                    buttonText: 'Okey',
                                    duration: 3000,
                                    type: 'warning',
                                    buttonTextStyle: { color: "white" },
                                    buttonStyle: { backgroundColor: "#388E3C" }
                                })
                            } else {
                                console.log('Rout to merchant detail', this.props)
                                Actions.merchantDetail({
                                    id: this.props.resId,
                                    title: this.props.resName,
                                    uri: this.props.resUri
                                });
                            }
                        }} />
                    </ListItem>
                    <Separator bordered>
                        <Label style={{fontSize: hp('2%')}}>{I18n.t('summary')}</Label>
                    </Separator>
                    <ListItem>
                        <Left>
                            <Text style={{fontSize: hp('1.5%')}}>{I18n.t('tempCost')}</Text>
                        </Left>
                        <Right>
                            <Text style={{fontSize: hp('1.5%')}}>{this.props.totalCost} VND</Text>
                        </Right>
                    </ListItem>
                    <ListItem>
                        <Left>
                            <Text style={{fontSize: hp('1.5%')}}>{I18n.t('deliveryCost')}</Text>
                        </Left>
                        <Right>
                            <Text style={{fontSize: hp('1.5%')}}>10 000 VND</Text>
                        </Right>
                    </ListItem>

                    <ListItem>
                        <Left>
                            <Text style={{fontSize: hp('1.5%')}}>{I18n.t('taxCost')}</Text>
                        </Left>
                        <Right>
                            <Text style={{fontSize: hp('1.5%')}}>{(this.props.totalCost) * 0.1} VND</Text>
                        </Right>
                    </ListItem>
                    <Separator bordered>
                        <ListItem>
                            <Left>
                                <Text style={{fontSize: hp('1.75%')}}>{I18n.t('totalCost')}</Text>
                            </Left>
                            <Right>
                                <Text style={{fontSize: hp('1.5%')}}>{parseInt(this.props.totalCost*1.1 + 10000)} VND</Text>
                            </Right>
                        </ListItem>
                    </Separator>
                    <ListItem style={{ height: 60 }}>
                        <BtnSubmit text={I18n.t('letTakeThem')} onPress={() => {
                            if (this.props.bill === null) {
                                Toast.show({
                                    text: I18n.t('youShouldPickSthFirst'),
                                    buttonText: 'Okey',
                                    duration: 3000,
                                    type: 'warning',
                                    buttonTextStyle: { color: "white" },
                                    buttonStyle: { backgroundColor: "#388E3C" }
                                })
                            } else {
                                this.showAlert();
                            }
                        }} />
                    </ListItem>
                </Content>
                {this.renderAlert(I18n.t('notification'), I18n.t('askToCreateOrder'))}
            </Container>
        );
    }
}

const mapStateToProps = state => {
    if (state.bill === null || state.bill === undefined) {
        return {
            bill: null,
            totalCost: 0,
            account: state.account
        }
    }
    let totalCost = 0;
    if (state.bill.items.length > 0)
        totalCost = state.bill.items.reduce((accumulator, currentItem) =>
            accumulator + currentItem.cost * currentItem.numbers, 0);
    // console.log('ACCOUT', state.account);
    return {
        bill: state.bill.items,
        totalCost: parseInt(totalCost),
        resId: state.bill.resId,
        resName: state.bill.resName,
        resUri: state.bill.resUri,
        account: state.account,
        token: state.token,
    }
}

export default connect(mapStateToProps)(Basket);