import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';
export const styleBasket = StyleSheet.create({
    summaryContainer: {
        borderTopWidth: 0.2,
        borderColor: '#707070',
        height: 50,
        width: '100%',
        marginBottom: 10,
        flex: 0,
        flexDirection: 'row',
        paddingLeft: 20,
        paddingRight: 20,
    },
    inputMoreDetail: {
        borderColor: '#707070',
        borderRadius: 10,
        borderWidth: 0.5,
        width: '100%',
        height: 35,
        fontSize: hp('1%'),
        marginTop: 10
    },
    basketTitleContainer: {
        height: 30,
        width: '100%',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    headerContainer: {
        height: 55,
        width: '100%',
        position: 'absolute',
        top: 0,
        borderBottomColor: '#000',
        borderBottomWidth: 0.5,
        borderRadius: 5,
        paddingLeft: 20,
        paddingRight: 20,
        shadowColor: '#707070',
        shadowOffset: { width: 1, height: 2 },
        elevation: 3
    },
    textTitle: {
        fontSize: hp('1.5%'),
        fontWeight: 'bold',
        color: '#707070'
    },
    textSmall: {
        fontSize: hp('1%'),
        color: '#707070'
    },
    cen_cen: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center'
    }
});