import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';

export class Item extends Component {
    render() {
        return (
            <View style={styleItem.itemContainer}>
                <View style={[{ width: '15%' }, styleItem.cen_cen]}>
                    <View style={styleItem.numberContainer}>
                        <Text style={{ fontSize: hp('1.5%'), fontWeight: 'bold', color: '#BD7207' }}>
                        {`${this.props.numbers}x`}
                        </Text>
                    </View>
                </View>

                <View style={[{ width: '70%' }, styleItem.cen_cen]}>
                    <Text style={styleItem.textSmall}>{this.props.name}</Text>
                </View>

                <View style={[{ width: '15%' }, styleItem.cen_cen]}>
                    <Text style={styleItem.textSmall}>{this.props.cost}</Text>
                </View>
            </View>
        );
    }
}

const styleItem = StyleSheet.create({
    textSmall: {
        fontSize: hp('1.8%'),
        color: '#707070'
    },
    cen_cen: {
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    itemContainer: {
        width: '100%',
        height: 41,
        backgroundColor: '#f8f8f8',
        flex: 1,
        flexDirection: 'row',
        marginBottom: 5
    },
    numberContainer: {
        height: 20,
        width: 20,
        borderRadius: 5,
        borderColor: '#707070',
        borderWidth: 1,
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center'
    }
})