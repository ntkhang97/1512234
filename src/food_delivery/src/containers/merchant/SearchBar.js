import React, {Component} from 'react';
import {TouchableOpacity, View, Image, TextInput, StyleSheet} from 'react-native';
import {getRestaurantsByName} from '../../services/api';
import Icon from 'react-native-vector-icons/FontAwesome';
import I18n from 'react-native-i18n';
export default class SearchBar extends Component {
    state={
        text: ''
    }
    render() {
        return (
            <View style={styleSearchBar.frame} pointerEvents={this.props.isDisable}>
                <View style={styleSearchBar.alignContent}>
                    <TextInput 
                    placeholder={I18n.t('searchRestaurant')}
                    style={{ paddingLeft: 20, paddingRight: 20, fontSize: 10, width: '80%' }}
                    value={this.state.text}
                    onChangeText={(text)=>{
                        this.setState({text: text})
                    }}
                    >
                        </TextInput>
                    <Icon.Button 
                    name='search' 
                    color='#aaa' 
                    backgroundColor='transparent' 
                    size={20}
                    onPress={()=>{
                        this.props.onPressSearch(this.state.text);
                        this.setState({text: ''})
                    }}
                    />
                </View>
            </View>
        );
    }
}

const styleSearchBar = StyleSheet.create({
    frame: {
        backgroundColor: 'white',
        borderRadius: 10,
        width: '100%',
        height: 35,
        opacity: 0.95
    },
    alignContent: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
});