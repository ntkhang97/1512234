import React, { Component } from 'react';
import Dialog, { DialogContent, DialogTitle, ScaleAnimation } from 'react-native-popup-dialog';
import ListRestaurants from '../../components/listRestaurants/ListRestaurants';
import { View, ActivityIndicator, Text, ScrollView } from 'react-native';
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';
export default class DialogRes extends Component {
    render() {
        return (
            <Dialog
                visible={this.props.isViewDialog}
                onTouchOutside={() => 
                // {
                //     this.setState({ isViewDialog: false });
                // }
                this.props.onTouchOutside()
                }
                width={0.9}
                height={0.6}
                dialogAnimation={new ScaleAnimation()}
                zIndex={1000}
                dialogTitle={<DialogTitle title='Các nhà hàng' align='center' />}
            >
                <DialogContent>
                    {this.props.isLoadingRes === true ?
                        <View style={{
                            backgroundColor: '#f5f5f5', height: '100%'
                            , display: 'flex', justifyContent: 'center', alignItems: 'center'
                        }}>
                            <ActivityIndicator size="large" color="#32936F" />
                        </View> :
                        this.props.isNoRes === true ? <Text>Khong tim thay nha hang</Text> :
                            <ScrollView style={{
                                backgroundColor: '#f5f5f5',
                                height: hp('40%')
                            }}>
                                <ListRestaurants dataSource={this.props.data} />
                            </ScrollView>
                    }
                </DialogContent>
            </Dialog>
        )
    }
}