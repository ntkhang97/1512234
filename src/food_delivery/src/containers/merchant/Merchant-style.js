import React from 'react';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    headContainer: {
        width: '100%',
        //  height: 100,
        flexGrow: 0,
        flexShrink: 1,
        flexBasis: 100,
    },
    topHeader: {
        height: '50%',
        flex: 0,
        justifyContent: 'center',
    },
    bottomHeader: {
        height: '50%',
    },
    contentContainer: {
        // height: '60%',
        flexGrow: 1,
        flexShrink: 1,
        flexBasis: 'auto',
    },
    headContent: {
        height: 65
    },
    mainContent: {
        height: '100%',
    },
    contentTitle: {
        fontFamily: 'gabriola',
        fontSize: 24,
        color: '#BD7207',
        textAlign: 'center',
        borderBottomWidth: 1.5,
        borderBottomColor: '#707070',
        marginLeft: 60,
        marginRight: 60
    },
    bottomContainer: {
        flexGrow: 0,
        flexShrink: 1,
        flexBasis: 70,
    },
});