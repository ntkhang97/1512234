import React, { Component } from 'react';
import { Text, View, Image, ActivityIndicator, Dimensions, StyleSheet, TouchableOpacity,Alert, ImageBackground, BackHandler } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { getRestaurantNearMe } from '../../services/api';
import Permissions from 'react-native-permissions';
import Dialog from './Dialog';
import I18n from 'react-native-i18n';
export default class Map extends Component {

    initialRegion = {
        latitude: 10.773533,
        longitude: 106.702899,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
    }

    state = {
        restaurants: [],
        isLoading: true,
        locationPermission: null,
        mapRegion: this.initialRegion,
        lastLat: null,
        lastLong: null,
        isViewDialog: false,
        isLoadingRes: false,
        res: null
    }

    componentDidMount() {
        Permissions.check('location').then(response => {
            // console.log('Location permission: ', response);
            this.setState({ locationPermission: response })
            if (response == 'undetermined' || response == 'restricted') {
                this._requestPermission();
            }
            else {
                // this.getCurrentLocation();
                // console.log('Get current location!!!')
                this.watchID = navigator.geolocation.watchPosition((position) => {
                    let region = {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: 0.00922 * 1.5,
                        longitudeDelta: 0.00421 * 1.5
                    }
                    this.onRegionChange(region, region.latitude, region.longitude);
                }, (error)=>console.log(error))
            }
        })
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    handleBackButtonClick() {
        Alert.alert(
            I18n.t('notification'),
            I18n.t('doExit'),
            [
                {
                    text: 'Ok', onPress: ()=>{
                        BackHandler.exitApp();
                    }
                },
                {
                    text: 'Cancel', style: 'cancel'
                }
            ],
            { cancelable: true }
        )
        return true;
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID);
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    _requestPermission = () => {
        Permissions.request('location').then(response => {
            // console.log('Location permission: ', response);
            this.setState({ locationPermission: response })

        })
    }

    loadRestaurant = async (latitude, longitude) => {
        const data = await getRestaurantNearMe(latitude, longitude);
        console.log(data);
        this.setState({ restaurants: data, isLoading: false });
    }

    handlePressRes = (res) =>{
        this.setState({
            isViewDialog: true,
            res: res
        })
    }

    showRestaurant = () => {
        if (this.state.restaurants.length === 0)
            return;
        let allLocations = [];
        allLocations = this.state.restaurants.map(res => {
            return <Marker
                key={res.id}
                coordinate={{ latitude: res.latitude, longitude: res.longitude }}
                title={res.RESTAURANT.name}
                onPress={(e)=>{
                    e.stopPropagation();
                    this.handlePressRes([res.RESTAURANT])
                }}
            >
                
                    <ImageBackground 
                        source={{uri: res.RESTAURANT.image}}
                        style={{width: 32, height: 32, borderRadius: '50%'}}
                        imageStyle={{ borderRadius: 16 }}
                    >
                    </ImageBackground>
            </Marker>
        })
        return allLocations;
    }

    getCurrentLocation = () => {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log("wokeeey");
                console.log(position);
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
        );
    }

    onRegionChange(region, lastLat, lastLong) {
        this.setState({
            mapRegion: region,
            // If there are no new values set the current ones
            lastLat: lastLat || this.state.lastLat,
            lastLong: lastLong || this.state.lastLong
        });
        console.log('Change location');
        this.loadRestaurant(this.state.lastLat, this.state.lastLong);
    }

    constructor(props) {
        super(props)
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    render() {
        return (
            <View style={styles.container}>
                {
                    this.state.isLoading === true ?
                    <View style={{
                        height: Dimensions.get('window').height-120, 
                        width: Dimensions.get('window').width, 
                        backgroundColor: 'white',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center'}}>
                        <ActivityIndicator size="large" color="#32936F" />
                    </View> :
                        <MapView
                            showsUserLocation={true}
                            followUserLocation={true}
                            style={styles.map}
                            provider={PROVIDER_GOOGLE}
                            onRegionChange={this.onRegionChange.bind(this)}
                            region={this.state.region}
                            mapType="standard"
                            region={this.state.mapRegion}
                            >
                            {this.showRestaurant()}
                            <MapView.Marker
                                coordinate={{
                                    latitude: (this.state.lastLat + 0.00050) || -36.82339,
                                    longitude: (this.state.lastLong + 0.00050) || -73.03569,
                                }}>
                                <View>
                                    <Text style={{ color: 'red' }}>
                                       {/* My location */}
                                    </Text>
                                </View>
                            </MapView.Marker>
                            <Dialog
                            isViewDialog={this.state.isViewDialog}
                            onTouchOutside={() => 
                                {
                                    this.setState({ isViewDialog: false });
                                }
                            }
                            isLoadingRes={this.state.isLoadingRes}
                            data={this.state.res}
                            />
                        </MapView>
                        
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        flex: 1,
        height: '100%',
        width: '100%',
        minHeight: Dimensions.get('window').height,
        minWidth: Dimensions.get('window').width,
    },
});