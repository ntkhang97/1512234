import React, {Component} from 'react';
import {TouchableOpacity, View, Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class CartBtn extends Component {
    render() {
        return (
            // <TouchableOpacity>
            //     <View>
            //         <Image source={require('../../icons/shoppingCart.png')} style={{ width: 30 }} />
            //     </View>
            // </TouchableOpacity>

            <Icon.Button name='shopping-cart' color='#fff' backgroundColor='transparent' size={28}/>
        );
    }
}