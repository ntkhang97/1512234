import React, { Component } from 'react';
import { StyleSheet, View, ActivityIndicator, Text, Dimensions, ImageBackground, 
    TouchableOpacity, ScrollView, Alert, BackHandler } from 'react-native';
import GridView from 'react-native-super-grid';
import LinearGradient from 'react-native-linear-gradient';
import { getAllCategory, getRestaurantsByCategory } from '../../services/api';
import Dialog, { DialogContent, DialogTitle, DialogButton, ScaleAnimation } from 'react-native-popup-dialog';
import ListRestaurants from '../../components/listRestaurants/ListRestaurants';
import { connect } from 'react-redux';
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';
import DialogRes from './Dialog';
import I18n from 'react-native-i18n';
class Category extends Component {

    state = {
        items: [],
        isLoading: true,
        isLoadingRes: false,
        isViewDialog: false,
        resIdSelected: 0,
        isNoRes: false,
        data: null,
    }
    componentDidMount = () => {
        this.getData();
    }

    getData = async () => {
        const data = await getAllCategory();
        console.log('load cateogry', data);
        data[0].image = 'https://www.bbcgoodfood.com/sites/default/files/guide/hub-image/2017/10/instant-expert-chinese-400x400.jpg';
        this.setState({ items: data, isLoading: false });
    }

    getRestaurantsData = async (id) => {
        const data = await getRestaurantsByCategory(id);
        console.log('res cate:', data);
        this.setState({data: data});
            // this.setState({isNoRes: true})
        this.setState({ isLoadingRes: false })
    }

    constructor(props) {
        super(props)
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    handleBackButtonClick() {
        Alert.alert(
            I18n.t('notification'),
            I18n.t('doExit'),
            [
                {
                    text: 'Ok', onPress: ()=>{
                        BackHandler.exitApp();
                    }
                },
                {
                    text: 'Cancel', style: 'cancel'
                }
            ],
            { cancelable: true }
        )
        return true;
    }

    render() {
        return (
            this.state.isLoading === true ?
                <View style={{
                    height: Dimensions.get('window').height-120, 
                    width: Dimensions.get('window').width, 
                    backgroundColor: 'white',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'}}>
                    <ActivityIndicator size="large" color="#32936F" />
                </View> :
                <View>
                    <GridView
                        itemDimension={Dimensions.get('window').width / 2 - 30}
                        items={this.state.items}
                        style={styles.gridView}
                        renderItem={item => (
                            <TouchableOpacity onPress={() => {
                                this.setState({ isLoadingRes: true });
                                this.setState({ isViewDialog: true, resIdSelected: item.id });
                                this.getRestaurantsData(item.id);
                            }}>
                                <ImageBackground source={item.image ? { uri: item.image } : require('./place03.jpg')}
                                    style={styles.itemContainer}>
                                    <LinearGradient
                                        colors={['transparent', '#000']}
                                        style={styles.content}>
                                        <Text style={[styles.itemName, { textAlign: 'center' }]}>{item.name.toUpperCase()}</Text>
                                    </LinearGradient>
                                </ImageBackground >
                            </TouchableOpacity>
                        )}
                    />
                    <DialogRes 
                    isViewDialog={this.state.isViewDialog}
                    onTouchOutside={() => 
                        {
                            this.setState({ isViewDialog: false });
                        }
                    }
                    isLoadingRes={this.state.isLoadingRes}
                    data={this.state.data}/>
                </View>
        );
    }
}

const styles = StyleSheet.create({
    gridView: {
        paddingTop: 25,
        flex: 1,
        backgroundColor: '#f5f5f5'
    },
    itemContainer: {
        justifyContent: 'flex-end',
        borderRadius: 10,
        height: Dimensions.get('window').width / 2 - 30,
        position: 'relative'
    },
    itemName: {
        fontSize: hp('2%'),
        color: '#fff',
        fontWeight: '600',
    },
    itemCode: {
        fontWeight: '600',
        fontSize: 12,
        color: '#fff',
    },
    content: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        height: 80,
        minHeight: 50,
        paddingTop: 5,
        paddingBottom: 10,
        paddingVertical: 10,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
});
const mapStateToProps = (state) => {
    return {
        dataSource: state.restaurants
    }
}
export default connect(mapStateToProps)(Category);