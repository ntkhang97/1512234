import React, { Component } from 'react';
import { Text, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";
import { ListItem, Title, Body, Subtitle, Radio, Left, Right, Icon, Button, Footer } from 'native-base';
import { Container, Header, Content } from 'native-base';
import {connect} from 'react-redux';
import I18n from 'react-native-i18n';
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
const COLOR = '#264653'
class Menu extends Component {
    render() {
        return (
            <Container>
                {/* <Header span
                    style={{
                        backgroundColor: '#388E3C',
                        flex: 0,
                        alignItems: 'center',
                        paddingLeft: 0,
                        paddingRight: 0,
                        maxHeight: 128
                    }}>
                    <Body style={{ flex: 0, justifyContent: 'center', height: '100%', width: '100%' }}>
                        <ImageBackground 
                        source={require('../../images/imgs/place02.jpg')} 
                        style={{ 
                            width: '100%', 
                            height: 128,
                            flex: 0,
                            alignItems: 'center',
                            justifyContent: 'center'}}>
                            <Title style={{
                                fontSize: hp('3%'),
                                fontFamily: 'gabriola'
                            }}>FOOD DELIVERY</Title>
                            <Subtitle style={{fontSize: hp('2%')}}>
                                {I18n.t('userName') + ': Nguyen Thinh Khang'}
                            </Subtitle>
                        </ImageBackground>

                    </Body>
                </Header> */}
                <Header style={{backgroundColor: COLOR}}>
                    <Body>
                        <Text style={{
                            color: 'white',
                            fontSize: hp('4%')
                        }}>Menu</Text>
                    </Body>
                </Header>
                <Content style={{backgroundColor: COLOR}}>
                    <Collapse>
                        <CollapseHeader style={{ backgroundColor: COLOR,display: 'flex', flexDirection: 'row' }}>
                            <Icon name='ios-code' style={{color: 'white', paddingLeft: 10,fontSize: wp('2.5%')}}/>
                            <Text style={menuStyle.menuHeader}>
                                {I18n.t('changeLanguage')}
                                </Text>
                        </CollapseHeader>
                        <CollapseBody>
                            <ListItem onPress={()=>{
                                    this.props.dispatch({type: 'CHANGE_LANGUAGE', language:'vn'})
                                    Alert.alert(
                                        I18n.t('notification'),
                                        I18n.t('changeLangSuccess'),
                                        [{
                                            text: 'OK', style: 'cancel'
                                        }],
                                        {cancelable: true}
                                    )
                                }}>
                                <Left>
                                    <Text style={menuStyle.menuText}>{I18n.t('vietnamese').toUpperCase()}</Text>
                                </Left>
                                <Right>
                                    <Radio selected={this.props.language==='vn'} color='white' selectedColor='#ffffff'/>
                                </Right>
                            </ListItem>
                            <ListItem last 
                            onPress={()=>{
                                this.props.dispatch({type: 'CHANGE_LANGUAGE', language:'en'})
                                Alert.alert(
                                    I18n.t('notification'),
                                        I18n.t('changeLangSuccess'),
                                    [{
                                        text: 'OK', style: 'cancel'
                                    }],
                                    {cancelable: true}
                                )
                            }}>
                                <Left>
                                    <Text style={menuStyle.menuText}>{I18n.t('english').toUpperCase()}</Text>
                                </Left>
                                <Right>
                                    <Radio selected={this.props.language==='en'} color='white' selectedColor='#ffffff'/>
                                </Right>
                            </ListItem>
                        </CollapseBody>
                    </Collapse>
                </Content>

                <Footer>
                    <Button block iconLeft 
                    style={{ backgroundColor: '#2A9D8F', height: '100%', width: '100%' }}
                        onPress={() => {
                            Alert.alert(
                                I18n.t('notification'),
                                I18n.t('doLogout'),
                                [
                                    {
                                        text: 'OK', onPress: () => {
                                            this.props.dispatch({ type: 'LOGIN', token: null });
                                            Actions.home();
                                        }
                                    },
                                    {
                                        text: 'Cancel', style: 'cancel'
                                    }
                                ],
                                { cancelable: true }
                            )
                        }}
                    >
                        <Icon name='ios-log-out' style={{fontSize: hp('2%')}} />
                        <Text style={[menuStyle.menuText, { color: 'white',fontSize: hp('2%') }]}>{I18n.t('logout')}</Text>
                    </Button>
                </Footer>
            </Container>
        )
    }
}

const menuStyle = StyleSheet.create({
    menuHeader: {
        textAlign: 'left',
        fontSize: hp('2%'),
        color: 'white',
        paddingLeft: 10
    },
    menuText: {
        color: 'white'
    }
})

const mapStateToProps = state => ({
    language: state.language
})
export default connect(mapStateToProps)(Menu);