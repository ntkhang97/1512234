import React, { Component } from 'react';
import { View, ImageBackground, ActivityIndicator, Dimensions, BackHandler, Alert } from 'react-native';
import { loadRestaurant, getAccountInfo } from '../../services/api';
import ListRestaurants from '../../components/listRestaurants/ListRestaurants';
import SearchBar from './SearchBar';
import { connect } from 'react-redux';
import Map from './Map';
import SideMenu from 'react-native-side-menu';
import Menu from './Menu';
import { Actions } from 'react-native-router-flux';
import Basket from '../basket/Basket';
import Category from './Category';
import {
    Container, Header, Content, Footer, FooterTab,
    Button, Icon, Text, Badge, Body, 
} from 'native-base';
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';
import SplashScreen from 'react-native-splash-screen';
import I18n from 'react-native-i18n';
import {getRestaurantsByName} from '../../services/api';
import DialogRes from './Dialog';
import Account from '../account/Account';
import Permissions from 'react-native-permissions';
const COLOR = '#32936F';

class Restaurants extends Component {
    constructor(props) {
        super(props)
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    handleBackButtonClick() {
        Alert.alert(
            I18n.t('notification'),
            I18n.t('doExit'),
            [
                {
                    text: 'Ok', onPress: ()=>{
                        BackHandler.exitApp();
                    }
                },
                {
                    text: 'Cancel', style: 'cancel'
                }
            ],
            { cancelable: true }
        )
        return true;
    }
    render(){
        return (
            <ListRestaurants isMerchant={true} dataSource={this.props.dataSource}/>
        )
    }
}

class Merchant extends Component {

    state = {
        activeTab: 'restaurant',
        isOpenMenu: false,
        isLoading: true,
        data: null,
        isLoadingRes: false,
        isViewDialog: false,
    }

    onScrollEndDrag() {
        console.log('Bottom of screen');
    }

    getContent = () => {
        const activeTab = this.state.activeTab;
        switch(activeTab){
            case 'restaurant':
                return !this.state.isLoading ? 
                <Restaurants dataSource={this.props.dataSource}/> : 
                <View style={{
                    height: Dimensions.get('window').height-120, 
                    width: Dimensions.get('window').width, 
                    backgroundColor: 'white',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'}}> 
                    <ActivityIndicator size="large" color={COLOR} />
                </View>
            case 'map':
                return <View>
                            <Map />
                        </View>
            case 'basket':
                return <View style={{flex: 0, height: hp('85%')}}>
                    <Basket/>
                </View>
            case 'account':
                return <View>
                    <Account/>
                </View>
            default: 
                return <Category/>
        }
    }

    componentDidMount(){
        this.loadData();
        SplashScreen.hide();
    }

    // Load data for list of restaurants
    loadData = async () => {
        const data = await loadRestaurant(10, 1);
        
        this.props.dispatch({ type: 'LOAD_RESTAURANTS', res: data });
        this.setState({ isLoading: false });
        const data2 = await getAccountInfo(this.props.token);
        // console.log('ACCOUNT DATA: ', data);
        this.props.dispatch({ type: 'GET_ACCOUNT', data: data2 });
        
    }

    renderTabBtn = tags => {
        return tags.map(tag => {
            if (tag.tagName === 'basket')
                return <Button style={{backgroundColor: COLOR}} key={tag.name} active={this.state.activeTab === tag.tagName} 
                badge vertical onPress={() => this.setState({ activeTab: tag.tagName })}>
                        <Badge><Text>{this.props.itemsInBasket}</Text></Badge>
                            <Icon name="ios-cart" color='white'/>
                            <Text style={{fontSize: wp('1.25%')}}>{I18n.t('basket')}</Text>
                        </Button>
        
            return (
                <Button style={{backgroundColor: COLOR}} key={tag.name} active={this.state.activeTab === tag.tagName} 
                vertical onPress={() => this.setState({ activeTab: tag.tagName })}>
                    <Icon name={tag.icon} color='white'/>
                    <Text style={{fontSize: wp('1.25%')}}>{tag.name}</Text>
                </Button>
            )
        })
    }

    handleSearch=async (text) => {
        this.setState({isViewDialog: true, isLoadingRes: true});
        const data = await getRestaurantsByName(text);
        this.setState({data: data});
        this.setState({isLoadingRes: false});
    }

    render() {
        const nmenu = <Menu />;
        const tags = [
            {
                tagName: 'restaurant',
                name: I18n.t('restaurant'),
                icon: 'ios-restaurant'
            },
            {
                tagName: 'categories',
                name: I18n.t('categories'),
                icon: 'ios-apps'
            },
            {
                tagName: 'map',
                name: I18n.t('map'),
                icon: 'ios-map'
            },
            {
                tagName: 'basket',
                name: I18n.t('basket'),
                icon: 'ios-cart'
            },
            {
                tagName: 'account',
                name: I18n.t('account'),
                icon: 'ios-person'
            }
        ]
        return (
            <SideMenu
                menu={nmenu}
                openMenuOffset={Dimensions.get('window').width * 0.4}
                isOpen={this.state.isOpenMenu}
                disableGestures={false}
                onChange={(isOpen)=>this.setState({isOpenMenu: isOpen})}>
                <Container>
                    <ImageBackground
                        source={require('../../images/imgs/screen01.png')}
                        style={{
                            width: '100%',
                            height: this.state.activeTab === 'restaurant' ? 60 : 0
                        }}>
                        <Header style={{ backgroundColor: 'transparent' }}>
                            <Button transparent
                                onPress={() => this.setState({ isOpenMenu: !this.state.isOpenMenu })}>
                                <Icon name='menu' />
                            </Button>
                            <Body>
                                <SearchBar 
                                isDisable={this.state.activeTab !== 'restaurant' ? 'none' : 'auto'} 
                                onPressSearch={(text=>{
                                    this.handleSearch(text);
                                })}
                                />
                            </Body>
                        </Header>
                    </ImageBackground>
                    <Content>
                        {this.getContent()}
                    </Content>

                    <Footer>
                        <FooterTab style={{ backgroundColor: COLOR, color:'white' }}>
                            {
                                this.renderTabBtn(tags)
                            }
                        </FooterTab>
                    </Footer>

                    <DialogRes 
                    isViewDialog={this.state.isViewDialog}
                    onTouchOutside={() => 
                        {
                            this.setState({ isViewDialog: false });
                        }
                    }
                    isLoadingRes={this.state.isLoadingRes}
                    data={this.state.data}/>
                </Container>
            </SideMenu>
        );
    }
}

const mapStateToProps = state => {
    let itemsInBasket = 0;
    if(state.bill !== undefined && state.bill !== null)
        itemsInBasket = state.bill.items.reduce((ac, cu)=>ac+cu.numbers, 0)
    return {
        itemsInBasket,
        dataSource: state.restaurants,
        token: state.token
    }
}

export default connect(mapStateToProps)(Merchant);