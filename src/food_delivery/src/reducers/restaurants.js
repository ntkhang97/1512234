import { LOAD_RESTAURANTS } from '../actions/define';

const initRestaurants = [];

export default restaurants = (state = initRestaurants, action) => {
    switch (action.type) {
        case LOAD_RESTAURANTS:
            return action.res;
        default: return state;
    }
}