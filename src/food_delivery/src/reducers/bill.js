import { ADD_ITEM, DELETE_ITEM, RESET_BILL } from '../actions/define';


const initState = null;
export default bill = (state = initState, action) => {
    switch (action.type) {
        case ADD_ITEM:
            //  console.log('in adding item: ');
            //  console.log('state',state);
            console.log('item adding',action.item);
             let res = null;
            // find in state
            if(state!==null)
                res = state.items.find(function (item) {
                    return item.id === action.item.id;
            });
            // not found
            // console.log('Res: ');
            // console.log(res);
            if (!res)
                if(state === null )
                    return {
                        resId: action.item.resId,
                        resName: action.item.resName,
                        resUri: action.item.resUri,
                        items: [
                            {
                                ...action.item,
                                numbers: 1
                            }
                        ]
                    };
                else
                    return {
                        resId: action.item.resId,
                        resName: action.item.resName,
                        resUri: action.item.resUri,
                        items: [
                            ...state.items,
                            {
                                ...action.item,
                                numbers: 1
                            }
                        ]
                    };
            // found
            else
                return {
                    resId: action.item.resId,
                    items: state.items.map(item => {
                        return (item.id === action.item.id) ? {
                            ...item,
                            numbers: item.numbers + 1
                        } : item
                    })
                }
        case DELETE_ITEM:
            // console.log(state);
            const res2 = state.items.find(function (item) {
                return item.id === action.item.id;
            });
            if (!res2)
                return state;
            else
                return {
                    resId: action.item.resId,
                    items: state.items.map(item => {
                        return (item.id === action.item.id && item.numbers > 0) ? {
                            ...item,
                            numbers: item.numbers - 1
                        } : item
                    }).filter(item=>{
                        return item.numbers > 0;
                    })
                }
        case RESET_BILL: {
            return initState;
        }
        default: return state;
    }
}