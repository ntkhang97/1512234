import {combineReducers} from 'redux';
import restaurants from './restaurants';
import menu from './menu';
import bill from './bill';
import language from './language';
import account from './account';
import token from './token';
export default combineReducers({
    language,
    restaurants,
    menu,
    bill,
    account,
    token
})