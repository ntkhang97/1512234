import { GET_ACCOUNT } from '../actions/define';
const initAccount = null
export default account = (state = initAccount, action) => {
    switch (action.type) {
        case GET_ACCOUNT: return action.data
        default: return state;
    }
}