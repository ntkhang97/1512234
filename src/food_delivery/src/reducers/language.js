import { CHANGE_LANGUAGE } from '../actions/define';
import I18n from 'react-native-i18n';
const initLanguage = 'vn'
export default language = (state = initLanguage, action) => {
    switch (action.type) {
        case CHANGE_LANGUAGE:
            I18n.locale=action.language;
            return action.language
        default: return state;
    }
}