import { LOGIN } from '../actions/define';
const initAccount = null
export default token = (state = initAccount, action) => {
    switch (action.type) {
        case LOGIN: return action.token
        default: return state;
    }
}