import { LOAD_MENU } from '../actions/define';

const initMenu = [
    {
        id: 'id',
        title: 'title',
        sold: 'sold',
        cost: 'cost'
    }
]
export default menu = (state = initMenu, action) => {
    switch (action.type) {
        case LOAD_MENU:
            // console.log('Loading menu: data');
            // console.log(action.menu);
            return action.menu
        default: return state;
    }
}