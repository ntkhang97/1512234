import React from 'react';
import { Actions } from 'react-native-router-flux';
import {Alert } from 'react-native';
import {saveUserToken, getUserToken} from './authentication';
import axios from 'axios';

function login(email, password, _this) {
    console.log(`handling submit: email: ${email} password: ${password}`);
        return fetch('https://food-delivery-server.herokuapp.com/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "email":email,
                "password":password
            })
        }).then(res=>{
            switch(res.status){
                case 200: 
                Alert.alert('Notification', 'Login Successful');
                break;
                case 400:
                Alert.alert('Notification', 'Email or Password is wrong');
                break;
                case 401:
                Alert.alert('Notification', 'Account not verified');
                break;
            }
            return res.json();
        }).then(res=>{
            if(res.token !== undefined) {
                console.log('login token in api', res.token);
                _this.props.dispatch({type: "LOGIN", token: res.token})
                Actions.merchant();
            }
        }).catch(e => console.log(e)).done();
}

function register(email, password) {
    console.log(`handling submit: email: ${email} password: ${password}`);
        fetch('https://food-delivery-server.herokuapp.com/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "email":email,
                "password":password
            })
        }).then(res=>{
            console.log(res);
            console.log(res.status);
            switch(res.status){
                case 200: 
                Alert.alert('Notification', 'Register Successful. You must check email to verify the account before loggin');
                Actions.login();
                break;
                case 400:
                Alert.alert('Notification', 'Email already exists');
                break;
                case 401:
                Alert.alert('Notification', 'Invalid email');
                break;
                case 500:
                Alert.alert('Notification', 'Other error');
                break;
            }
            return res.json();
        }).then(res=>{}).catch(e => console.log(e)).done();
}

function forgetPassword(email) {
    fetch('https://food-delivery-server.herokuapp.com/forgetPassword', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "email":email,
            })
        }).then(res=>{
            console.log(res);
            console.log(res.status);
            switch(res.status){
                case 200: 
                Alert.alert('Notification', `Reset password successfully, an email has been sent to ${email}`);
                Actions.login();
                break;
                case 400:
                Alert.alert('Notification', 'email is wrong');
                break;
                case 500:
                Alert.alert('Notification', 'Other error');
                break;
            }
            return res.json();
        }).then(res=>{}).catch(e => console.log(e)).done();
}

function loadRestaurant(perpage, page){
    return axios.get(`https://food-delivery-server.herokuapp.com/restaurant/getAll/${perpage}&${page}`).then(res=>{   
        // console.log('In loading restaurants');
    // return res.data.data.map(d=>{
    //         let info = d.info;
    //         return {
    //             id: info.id,
    //             title: info.name,
    //             idAddress: info.idAddress,
    //             rating: info.rating,
    //             image: info.image,
    //             timeOpen: info.timeOpen,
    //             timeClose: info.timeClose
    //         }
    //     });
    return res.data.data.map(d=>d.info);
        //_this.props.dispatch({type: 'LOAD_RESTAURANTS',res: data});
    }).catch(e=>console.log(e));
}

function loadMenuInRestaurant(id){
    return axios.get(`https://food-delivery-server.herokuapp.com/restaurant/getMenu/${id}`).then(res=>{  
    return res.data.menu.map(d=>{
            return {
                resId: id,
                resName: res.data.restaurant.name,
                resUri: res.data.restaurant.image,
                id: d.id,
                title: d.name,
                image: d.image,
                sold: d.sold,
                cost: d.price,
            }
        });
        // console.log(data);
        // _this.props.dispatch({type: 'LOAD_MENU', menu: data});
    }).catch(e=>console.log(e));
}

function getRestaurantNearMe(lat, long) {
    return axios.get(`https://food-delivery-server.herokuapp.com/restaurant/nearMe/${lat}&${long}`).then(res=>{
        return res.data;
    });
}

function getAllCategory() {
    return axios.get('https://food-delivery-server.herokuapp.com/categories/getAll').then(res=>{
        console.log('API LOADING CATEGORY:', res.data);
        return res.data;
    });
}

function getRestaurantsByCategory(id){
    return axios.get(`https://food-delivery-server.herokuapp.com/restaurant/getCategory/${id}`).then(res=>{
    // console.log('in api get res cate', res);    
    return res.data
    })
}

function getRestaurantsByName(name){
    return axios.get(`https://food-delivery-server.herokuapp.com/restaurant/search?name=${name}`).then(res=>{
        console.log('in api res name', res.data);
        return res.data;
    })
}

async function getAccountInfo(token) {
    let URL = 'https://food-delivery-server.herokuapp.com/getinfo'
    return await axios.get(URL, {
        headers: {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoidGhpbmhraGFuZzk3QGdtYWlsLmNvbSIsImlhdCI6MTU0NTEwNjc5MywiZXhwIjoxNTQ3Njk4NzkzfQ.iQ0zjPCCYxcqpQIVQmdDUSUsEnTVpypaKeYujoGLWPw'
        }
    }).then(res=>{
        return res.data;
    })
}

async function getAllDistrict() {
    let URL = 'https://food-delivery-server.herokuapp.com/district/getAll';
    return await axios.get(URL).then(res=>{
        return res.data;
    })
}

async function getAllWards(id) {
    let URL = `https://food-delivery-server.herokuapp.com/ward/getAllByDistrict?id=${id}`;
    return await axios.get(URL).then(res=>{
        return res.data;
    })
}

function updateAccountInfo(token,username, phone) {
    let data = {
        userName: username,
        phone
    }
    let headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
    let URL = `https://food-delivery-server.herokuapp.com/updateInfo`
    return axios.post(URL,data,{
        headers: headers
    })
}

function updateAddressInfo(token,idDistrict, ward, street) {
    let data = {
        idDistrict: parseInt(idDistrict),
        idWard: parseInt(ward),
        street
    }
    let headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
    let URL = `https://food-delivery-server.herokuapp.com/updateInfo`
    return axios.post(URL,data,{
        headers: headers
    })
}

function updateAddressPassword(token, newPassword) {
    let data = {
        password: newPassword
    }
    let headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
    let URL = `https://food-delivery-server.herokuapp.com/updatePassword`
    return axios.post(URL,data,{
        headers: headers
    })
}

async function getDistrictName(id) {
    let URL = `https://food-delivery-server.herokuapp.com/district/getAll`;
    // console.log('Get ward of id', idDistrict);
    return await axios.get(URL).then(res=>{
        let allData = res.data;
        let name = '';
        allData.map(d=>{
            if(d.id == id)
                name = d.name;
        })
        return name;
    })
}

async function getWardName(id, idDistrict) {
    let URL = `https://food-delivery-server.herokuapp.com/ward/getAllByDistrict?id=${idDistrict}`;
    console.log('Get ward of id', idDistrict);
    return await axios.get(URL).then(res=>{
        let allData = res.data;
        let name = '';
        allData.map(w=>{
            if(w.id == id)
                name = w.name;
        })
        return name;
    })
}

function createOrder(token, data) {
    let headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
    let URL = `https://food-delivery-server.herokuapp.com/order/create`;
    return axios.post(URL, data, {headers: headers}).then(res=>{
        return res
    })
}

function getAllOrder(token) {
    let headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
    let URL = `https://food-delivery-server.herokuapp.com/order/getAll`;
    return axios.get(URL, {headers: headers}).then(res=>{
        return res.data
    })
}

function getOrderDetail(token, id) {
    let headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
    let URL = `https://food-delivery-server.herokuapp.com/order/getOrder/${id}`;
    console.log('TOKEN IN API',token, id);
    return axios.get(URL, {headers: headers}).then(res=>{
        console.log('LOADED DATA', res.data);
        return res.data
    })
}

function getFoodById(id) {
    let URL =  `https://food-delivery-server.herokuapp.com/food/${id}`;
    return axios.get(URL).then(res=>{
        return res.data
    })
}

function getAllComment(id) {
    let URL = `https://food-delivery-server.herokuapp.com/restaurant/getAllCommentById/${id}`;
    return axios.get(URL).then(res=>{
        return res.data;
    })
}

function addComment(id, userName, content) {
    let URL = `https://food-delivery-server.herokuapp.com/restaurant/addComment`;
    let data = {
        name: userName,
        idRestaurant: parseInt(id),
        content: content
    }
    return axios.post(URL,data);
}

export {
    login, register, loadRestaurant, loadMenuInRestaurant, getRestaurantNearMe,
    getAllCategory, getRestaurantsByCategory, forgetPassword, getRestaurantsByName,
    getAccountInfo, getAllDistrict, getAllWards, updateAccountInfo, updateAddressInfo,
    updateAddressPassword, getDistrictName, getWardName, createOrder, getAllOrder,
    getOrderDetail, getFoodById, getAllComment, addComment
};