import React, { Component } from 'react';
import { Alert, AsyncStorage } from 'react-native';

async function saveUserToken(token) {
    try {
        await AsyncStorage.setItem('userToken', token);
    } catch { console.log('Save token error'); }
}

async function getUserToken() {
    let token ='';
    try {
        token = await AsyncStorage.getItem('userToken')||'none';
    } catch{
        console.log('Get token error');
    }
    return token;
}
export {saveUserToken, getUserToken};