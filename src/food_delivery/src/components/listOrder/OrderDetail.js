import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import {getAllOrder, getOrderDetail, getFoodById} from '../../services/api'
import { 
    Container, Header, Content, List, ListItem, Left, Body, 
    Right, Thumbnail, Text, Title, Button, Icon, Badge
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import I18n from 'react-native-i18n';
const COLOR = '#32936F'
class OrderDetail extends Component {

    state = {
        listFood: []
    }

    componentDidMount() {
        this.loadOrderDetail(this.props.token, this.props.id);
    }

    loadOrderDetail = async (token, id)=>{
        //console.log('TOKEN', token);
        if(token) {
            const data = await getOrderDetail(token,id);
            //console.log('ORDER DETAIL',data);
            let listFood = [];
            for(let i = 0; i < data.details.length; i++) {
                const foodInfo = await getFoodById(data.details[i].idFood);
                //console.log('FOOD GET', foodInfo);
                listFood.push({
                    name: foodInfo.food.name,
                    price: foodInfo.food.price,
                    image: foodInfo.food.image,
                    quantity: data.details[i].quantity,
                    note: data.details[i].note
                })
            }
            //console.log('LOADED ORDER DETAIL:', listFood);
            this.setState({listFood: listFood});
        }
        return [];
    }

    getListOrderDetail() {
        let list = this.state.listFood;
        // console.log('LIST', list);
        if(list.length>0) {
            return list.map(l=>{
                return <ListItem thumbnail key={l.id}>
                <Left>
                    <Thumbnail square source={{ uri: l.image}} />
                </Left>
                <Body>
                    <Text style={{color: COLOR}}>{`Name: ${l.name}`}</Text>
                    <Text note>{`Price: ${l.price}, Note: ${l.note}`}</Text>
                </Body>
                <Right>
                    <Badge success>
                        <Text>{`${l.quantity}`}</Text>
                    </Badge>
                </Right>
            </ListItem>
            })
        }
    }

    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: COLOR }}>
                    <Body>
                        <Title>{I18n.t('orderDetail')}</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => Actions.merchant()}>
                            <Icon name='home' />
                        </Button>
                    </Right>
                </Header>
                <Content>
                    {this.getListOrderDetail()}
                </Content>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        token: state.token
    }
}
export default connect()(OrderDetail);