import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import {getAllOrder, getOrderDetail, getFoodById} from '../../services/api'
import { 
    Container, Header, Content, List, ListItem, Left, Body, 
    Right, Thumbnail, Text, Title, Button, Icon
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import moment from 'moment'
import I18n from 'react-native-i18n';
const COLOR = '#32936F'
class ListOrder extends Component {

    state = {
        listOrder: []
    }

    componentDidMount() {
        this.loadListOrder();
    }

    loadListOrder = async () => {
        const token = this.props.token;
        if(token) {
            const data = await getAllOrder(token);
            const processedData = data.map(d=>{
                return {
                    totalPrice: d.totalPrice,
                    time: moment(d.date).fromNow(),
                    address: d.address,
                    phone: d.phone,
                    id: d.id
                }
            });
            this.setState({listOrder: processedData})
        }
    }

    getListOrder() {
        let list = this.state.listOrder;
        console.log('LIST', list);
        if(list.length>0) {
            return list.map(l=>{
                return <ListItem avatar key={l.id}>
                <Body>
                    <TouchableOpacity onPress={()=>{
                    Actions.orderDetail({id: l.id, token: this.props.token })
                }}>
                        <Text style={{color: COLOR}}>{`Order to: ${l.address}, phone: ${l.phone}`}</Text>
                        <Text note>{`Cost: ${l.totalPrice}, Time: ${l.time}`}</Text>
                    </TouchableOpacity>
                </Body>
            </ListItem>
            })
        }
    }

    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: COLOR }}>
                    <Body>
                        <Title>{I18n.t('listOrder')}</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => Actions.merchant()}>
                            <Icon name='home' />
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <List>
                        {this.getListOrder()}
                    </List>
                </Content>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        token: state.token
    }
}
export default connect(mapStateToProps)(ListOrder);