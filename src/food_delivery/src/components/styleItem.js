import React from 'react';
import { StyleSheet} from 'react-native';
const COLOR = '#BD7207'
export const styleItem = StyleSheet.create({
    frame: {
        height: 85,
        width: '100%',
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingLeft: 20,
        paddingRight: 20,
        marginTop: 10,
        marginBottom: 10
    },
    imageFrame: {
        width: '20%',
        height: '100%',
        flex: 0,
        flexDirection: 'row',
        justifyContent: "flex-end",
    },
    image: {
        width: 85,
        height: 85,
        borderRadius: 8,
    },
    contentFrame: {
        width: '60%',
        height: 85,
        borderBottomColor: '#707070',
        borderBottomWidth: 0.5,
    },
    contentTitle: {
        color: COLOR,
        fontWeight: 'bold',
        fontSize: 14
    },
    contentKind: {
        color: '#707070',
        fontSize: 8,
        paddingTop: 5
    },
    contentTime: {
        color: '#707070',
        fontSize: 10,
        fontWeight: 'bold'
    }
});