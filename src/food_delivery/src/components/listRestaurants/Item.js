import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {styleItem} from '../styleItem';

export class Item extends Component {

    handleTouchView(data) {
        // console.log('In handling touch view, data');
        // console.log(data);
        Actions.merchantDetail(data);
    }

    render() {
        return (
            <TouchableOpacity
                onPress={() => this.handleTouchView(
                    {
                        id: this.props.id,
                        title: this.props.title,
                        uri: this.props.uri
                    }
                )}>
                <View style={styleItem.frame}>
                    <View style={styleItem.imageFrame}>
                        <Image style={styleItem.image}
                            source={this.props.uri ? { uri: this.props.uri } : require('../../images/imgs/place04.png')}
                            resizeMethod='resize' />
                    </View>

                    <View style={styleItem.contentFrame}>
                        <View style={{
                            height: 18,
                        }}>
                            <Text style={styleItem.contentTitle}>{this.props.title ? this.props.title : 'KFC-135/2 XVNT'}</Text>
                        </View>
                        <View style={{
                            height: 16,
                            marginTop: 20
                        }}>
                            <Text style={styleItem.contentKind}>{this.props.kind ? this.props.kind : 'Fast food'}</Text>
                        </View>
                        <View style={{
                            height: 16,
                            marginTop: 5
                        }}>
                            <Text style={styleItem.contentTime}>{this.props.time ? this.props.time : '>60 min'}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}