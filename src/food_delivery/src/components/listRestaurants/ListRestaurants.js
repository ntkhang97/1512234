import React, { Component } from 'react';
import { ListView, View, Dimensions } from 'react-native';
import { connect } from 'react-redux';
// import { Item } from './Item';
import ResItem from './resItem';
import InfiniteScrollView from 'react-native-infinite-scroll-view';
var ds = new ListView.DataSource({ rowHasChanged: (row) => r1 !== r2 });
class ListRestaurants extends Component {

    scrollTo = (y) => {
        const deviceHeight = Dimensions.get('window').height;
        if (y < deviceHeight - 120) {
            this.refs.listView.scrollTo({ y: 0, animated: true })
            console.log('REACH BOTTOM');
        } else {
            let bottomSpacing = 180;
            if (this.state.messages.length > 0) {
                bottomSpacing = 120;
            }

            this.refs.listView.scrollTo({ y: y - deviceHeight + bottomSpacing, animated: true })
        }
    }
    render() {
        return (
            <ListView
                ref='listView'
                dataSource={this.props.dataSource ? ds.cloneWithRows(this.props.dataSource) : ds}
                onContentSizeChange={(contentWidth, contentHeight) => {
                    //console.log(contentWidth, contentHeight);
                    //this.scrollTo(contentHeight);
                }}
                renderRow={(data) =>
                    <ResItem
                        isMerchant={this.props.isMerchant}
                        title={data.name}
                        kind={data.kind}
                        time={data.time}
                        uri={data.image}
                        id={data.id}
                        rating={data.rating} />}
            >
            </ListView>
        );
    }
}

// const mapStateToProps = (state) => {
//     return {
//         dataSource: state.restaurants
//     }
// }

export default ListRestaurants;
