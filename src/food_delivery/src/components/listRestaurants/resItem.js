import React, { Component } from 'react';
import { View, StyleSheet, Text, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import ListComment from './listComment';
import {getAllComment} from '../../services/api';
const COLOR = '#00171F';

const Star = (type, key) => {
    switch (type) {
        case 'full':
            return <Icon key={key} size={hp('3%')} color='#FFBC04' name='star' />;
        case 'half':
            return <Icon key={key} size={hp('3%')} color='#FFBC04' name='star-half-o' />;
        case 'empty':
            return <Icon key={key} size={hp('3%')} color='#FFBC04' name='star-o' />;
    }
}


class ResItem extends Component {

    state = {
        listComment: []
    }
    getStar(rate) {
        const intRate = Math.round(rate) - 1;
        const haft = rate - intRate;
        let result = [];
        let i = 0
        for (; i < intRate; i = i + 1) {
            result.push(Star('full', i))
        }
        if (haft > 0)
            result.push(Star('half', i))
        for (let j = intRate + 1; j < 5; j = j + 1) {
            result.push(Star('empty', j))
        }
        return result;
    }

    handleTouchView(data) {
        // console.log('In handling touch view, data');
        // console.log(data);
        Actions.merchantDetail(data);
    }

    componentDidMount() {
        this.loadAllComment();
    }

    loadAllComment = async () =>{
        const id = this.props.id;
        if(id) {
            const tmp = await getAllComment(id);
            const data = tmp.result;
            this.setState({listComment: data});
        }
    }

    render() {
        return (
            <View>
                <TouchableOpacity style={ItemStyle.container}
                    onPress={() => this.handleTouchView(
                        {
                            id: this.props.id,
                            title: this.props.title,
                            uri: this.props.uri
                        }
                    )}>
                    <ImageBackground source={this.props.uri ? { uri: this.props.uri } : require('../../images/imgs/place04.png')}
                        style={ItemStyle.image} resizeMethod='resize'>
                        <LinearGradient
                            colors={['transparent', '#000']}
                            style={ItemStyle.content}>
                            <View style={ItemStyle.row}>
                                <View style={[ItemStyle.rowPart, ItemStyle.leftPart]}>
                                    <Text style={[ItemStyle.title,
                                    { fontSize: hp('2.2%') }]}>{this.props.title ? this.props.title : 'KFC-135/2 XVNT'}</Text>
                                </View>
                                <View style={[ItemStyle.rowPart, ItemStyle.right, ItemStyle.star]}>
                                    {this.getStar(this.props.rating ? this.props.rating : 3.5)}
                                </View>
                            </View>
                            <View style={ItemStyle.row}>
                                <View style={[ItemStyle.rowPart, ItemStyle.leftPart]} >
                                    <Text style={ItemStyle.title}>{this.props.kind ? this.props.kind : 'Fast food'}</Text>
                                </View>
                                <View style={[ItemStyle.rowPart, ItemStyle.right]}>
                                    <Text style={[ItemStyle.title, { fontSize: hp('1.5%') }]}>{this.props.time ? this.props.time : '<60 min'}</Text>
                                </View>
                            </View>
                        </LinearGradient>
                    </ImageBackground>
                </TouchableOpacity>
                <View style={{
                    paddingHorizontal: 10,
                    }}>
                    {
                        this.props.isMerchant === true ? 
                    <View style={{
                        backgroundColor: COLOR}}>
                        <ListComment data={this.state.listComment} id={this.props.id}/>
                    </View> : <View/>
                    }
                </View>
                
            </View>
        );
    }
}
const ItemStyle = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        height: Dimensions.get('window').width * 3 / 4,
        maxHeight: 500,
        maxWidth: '100%',
        shadowOffset: { width: 20, height: 20, },
        shadowColor: 'black',
        shadowOpacity: 1.0,
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    image: {
        width: '100%',
        height: Dimensions.get('window').width * 3 / 4,
        minHeight: 200,
        position: 'relative',
    },
    content: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        height: hp('20%'),
        minHeight: 70,
        paddingTop: 5,
        paddingBottom: 10,
        paddingVertical: 10
    },
    row: {
        width: '100%',
        height: '50%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        overflow: 'hidden'
    },
    rowPart: {
        height: '100%',
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10
    },
    leftPart: {
        width: '70%',
    },
    right: {
        width: '20%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    title: {
        color: 'white'
    },
    star: {

    }
})
export default ResItem;