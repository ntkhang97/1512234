import React, { Component } from 'react';
import { View, TouchableOpacity, Alert } from 'react-native';
import {addComment } from '../../services/api'
import {
    ListItem, Title, Body, Separator, Form, Input, Left, Icon,
    Right, List, Button, Header, Content, Label, Textarea, Container, Item, Text,
    Picker
} from 'native-base';
import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import moment from 'moment';
import I18n from 'react-native-i18n';
const COLOR = '#32936F';

class ListComment extends Component {
    state={
        content: ''
    }
    getListComment() {
        if(this.props.data.length>0)
        return this.props.data.map(l=>{
            return <ListItem thumbnail key={l.id}>
            <Left>
                <Icon active name='ios-chatboxes' style={{ color: 'white' }} />
            </Left>
            <Body>
                <Text style={{color: 'white'}}>{`${l.content}`}</Text>
                <Text note style={{color: '#505050'}}>{`${l.name} - ${moment(l.createAt).fromNow()}`}</Text>
            </Body>
        </ListItem>
        })
    }
    render() {
        return (
            <Collapse isCollapsed={this.state.content !== ''}>
                <CollapseHeader>
                    <Text style={{color: 'white', marginLeft: 10}}>{I18n.t('comment')}</Text>
                </CollapseHeader>

                <CollapseBody>
                    <Item>
                        <Input 
                        style={{color: 'white'}} 
                        placeholder={I18n.t('commentPlaceHolder')}
                        value={this.state.content}
                        onChangeText={(text)=>{
                            this.setState({content: text})
                        }}
                        />
                        <Icon active style={{color: 'white'}} 
                        name='ios-send' 
                        onPress={()=>{
                            addComment(this.props.id, this.props.account.userName,this.state.content).then(res=>{
                                console.log('RESULT OF ADD COMMENT', res);
                                Alert.alert('Notification', res.data.msg);
                            })
                        }}
                        />
                    </Item>
                    {this.getListComment()}
                </CollapseBody>
            </Collapse>
        )
    }
}
const mapStateToProps = state => {
    // console.log('ACCOUNT', state.account);
    return {
        account: state.account
    }
}
export default connect(mapStateToProps)(ListComment);