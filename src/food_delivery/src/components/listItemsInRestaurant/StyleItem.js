import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';
const COLOR = '#BD7207'
export default styleItem = StyleSheet.create({
    frame: {
        backgroundColor: 'white',
        height: 85,
        width: '100%',
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 20,
        paddingRight: 20,
        marginTop: 10,
        marginBottom: 10
    },
    imageFrame: {
        width: 85,
        height: 85,
        borderRadius: 8,
    },
    contentFrame: {
        width: 220,
        height: 85,
        borderBottomColor: '#707070',
        borderBottomWidth: 0.5
    },
    contentTitle: {
        color: COLOR,
        fontWeight: 'bold',
        fontSize: hp('1.5%')
    },
    contentKind: {
        color: '#707070',
        fontSize: hp('1.5%'),
        paddingTop: 5
    },
    contentTime: {
        color: '#707070',
        fontSize: hp('1%'),
        fontWeight: 'bold'
    }
});