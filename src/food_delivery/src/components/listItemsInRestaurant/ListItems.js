import React, { Component } from 'react';
import { ListView, View } from 'react-native';
import Item from './Item';
import {loadMenuInRestaurant} from '../../services/api';
import {connect} from 'react-redux';

var ds = new ListView.DataSource({ rowHasChanged: (row) => r1 !== r2 });
class ListItems extends Component {
    render() {
        return (
            // <View>

            // </View>
            <ListView
                dataSource={this.props.dataSource}
                renderRow={(data) => 
                <Item 
                resId={this.props.id}
                resName={data.resName}
                resUri={data.resUri} 
                id={data.id} 
                title={data.title} 
                sold={data.sold} 
                cost={data.cost} 
                uri={data.image} 
                numbers={data.numbers}/>}>
            </ListView>
        );
    }
}

const mapStateToProps = state=>{
    const menu = state.menu;
    let listItem = listItem = menu.map(item=>{
        return{
            ...item,
            numbers: 0
        }
    });
    // console.log('In list item: data ', listItem);
    return {
        dataSource: ds.cloneWithRows(listItem)
    }
}
export default connect(mapStateToProps)(ListItems);