import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import {connect} from 'react-redux';
import {addItem, deleteItem} from '../../actions/index';
class NumberBar extends Component {
    constructor(props) {
        super(props);
        // console.log('numbers of item: ', this.props.item.numbers)
        this.state = {
            count: 0
        }
    }
    
    handleAddItem() {
        /// Alert user to delete the old bill
        if(this.props.resInBill !== null && this.props.item.resId !== this.props.resInBill ) {
            Alert.alert(
                'Notification',
                'You have to remove all items before you pick a new foods. Do you want to do that?',
                [
                    {text: 'Cancel', style: 'cancel'},
                    {text: 'OK', onPress: ()=>
                    this.props.dispatch({type: 'RESET_BILL'})}
                ]
            )
        } else {
            this.setState({count: this.state.count+1});
            const item = {
                resId: this.props.item.resId,
                resName: this.props.item.resName,
                resUri: this.props.item.resUri,
                id: this.props.item.id,
                title: this.props.item.title,
                cost: this.props.item.cost
            };
            this.props.dispatch(addItem(item))
        }
    }

    handleDeleteItem() {
        if (this.state.count < 1)
            return;

        const item = {
            resId: this.props.item.resId,
            id: this.props.item.id,
            title: this.props.item.title,
            cost: this.props.item.cost
        };
        this.setState({count: this.state.count-1});
        this.props.dispatch(deleteItem(item));
    }

    componentDidMount() {
        const items = this.props.itemsInBill;
        console.log('In number bar cdm:', items, this.props.item);
        if(items!== null){
            const it = items.filter(i=>i.id === this.props.item.id);
            console.log('Found: ', it);
            if(it.length>0){
                this.setState({count: it[0].numbers});
                console.log('Set: ', it[0].numbers);
            }
                
        }
    }

    render() {
        return (
            <View style={styleNumberbar.frame}>
                <Btn text='-' onPress={() => this.handleDeleteItem()}></Btn>
                <Text style={{ color: '#707070' }}>{this.state.count}</Text>
                <Btn text='+' onPress={() => this.handleAddItem()}></Btn>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        resInBill: state.bill === null ? null : state.bill.resId,
        itemsInBill: state.bill === null ? null : state.bill.items
    }
}
export default connect(mapStateToProps)(NumberBar);

const styleNumberbar = StyleSheet.create({
    frame: {
        height: '100%',
        width: 50,
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    }
});

class Btn extends Component {
    render() {
        return (
            <TouchableOpacity onPress={() => this.props.onPress()}>
                <View>
                    <Text style={styleBtn.textStyle}>{this.props.text}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styleBtn = StyleSheet.create({
    textStyle: {
        color: '#BD7207',
        fontSize: 26,
        fontWeight: 'bold'
    }
});