import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {styleItem} from '../styleItem';
import  NumberBar  from './NumberBar';
import {connect} from 'react-redux';

class Item extends Component {

    handleTouchView(data) {
        Actions.merchantDetail(data);
    }

    render() {
        return (
            <View style={styleItem.frame}>
                <View style={styleItem.imageFrame}>
                    <Image style={styleItem.image}
                        source={this.props.uri ? { uri: this.props.uri } : require('../../images/imgs/place04.png')}
                        resizeMethod='resize' />
                </View>

                <View style={styleItem.contentFrame}>
                    <View style={{
                        height: 18,
                    }}>
                        <Text style={styleItem.contentTitle}>{this.props.title ? this.props.title : 'Combo: 2 chickens'}</Text>
                    </View>
                    <View style={{
                        height: 16,
                        marginTop: 20
                    }}>
                        <Text style={styleItem.contentKind}>Sold: {this.props.sold ? this.props.sold : '24'}</Text>
                    </View>
                    <View style={{
                        height: 16,
                        marginTop: 5,
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                    }}>
                        <Text style={styleItem.contentTime}>Cost: {this.props.cost ? this.props.cost : '20 000 '} VND</Text>
                        <NumberBar 
                        item = {{
                            id: this.props.id, 
                            title: this.props.title, 
                            cost: this.props.cost, 
                            resId: this.props.resId,
                            resName: this.props.resName,
                            resUri: this.props.resUri,
                            numbers: this.props.numbers
                            }}>
                            </NumberBar>
                    </View>
                </View>
            </View>

        );
    }
}

export default connect()(Item);