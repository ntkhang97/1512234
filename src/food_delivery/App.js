import React, { Component } from 'react';
import Home from './src/containers/home/Home';
import  Login  from './src/containers/authentication/Login';
import { ForgetPassword } from './src/containers/authentication/ForgetPassword';
import { Register } from './src/containers/authentication/Register';
import Merchant from './src/containers/merchant/Merchant';
import MerchantDetail from './src/containers/merchantDetail/MerchantDetail';
import Basket from './src/containers/basket/Basket';
import ListOrder from './src/components/listOrder/ListOrder';
import OrderDetail from './src/components/listOrder/OrderDetail';
import { Router, Scene, Drawer } from 'react-native-router-flux';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducers from './src/reducers/index';
import I18n from 'react-native-i18n';
import vn from './src/languages/vn';
import en from './src/languages/en';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { PersistGate } from 'redux-persist/integration/react'
import { Root } from 'native-base';
import Account from './src/containers/account/Account';
I18n.fallbacks = true;
I18n.translations = {
  vn,
  en
}
I18n.locale = 'vn';

const persistConfig = {
  timeout: 10000,
  key: 'root',
  storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducers)
const store = createStore(persistedReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
const persistor = persistStore(store);

export default class FoodDeliveryApp extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Root>
            <Router sceneStyle={{ backgroundColor: 'white' }}>
              <Scene key='root'>
                <Scene key='home' component={Home} initial hideNavBar={true}>
                </Scene>

                <Scene key='login' component={Login} hideNavBar={true}>
                </Scene>

                <Scene key='register' component={Register} hideNavBar={true}>
                </Scene>

                <Scene key='forgetPassword' component={ForgetPassword} hideNavBar={true}>
                </Scene>

                <Scene key='merchant' component={Merchant} hideNavBar={true}>
                </Scene>

                <Scene key='merchantDetail' component={MerchantDetail} hideNavBar={true}>
                </Scene>

                <Scene key='basket' component={Basket} hideNavBar={true}>
                </Scene>

                <Scene key='account' component={Account} hideNavBar={true}>
                </Scene>

                <Scene key='listOrder' component={ListOrder} hideNavBar={true}>
                </Scene>

                <Scene key='orderDetail' component={OrderDetail} hideNavBar={true}>
                </Scene>
              </Scene>
            </Router>
          </Root>
        </PersistGate>
      </Provider>
      // <Account/>
    );
  }
}